package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesDeturn;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesDeturnService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:12
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesDeturnController extends BaseController {
    @Inject
    private CasesDeturnService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesDeturn(){
        UploadFile uploadFile=this.getFile();
        YCasesDeturn ycase=this.getModel(YCasesDeturn.class,"casesdeturn");
        if(service.ins_YCasesDeturn(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesDeturn(){
        UploadFile uploadFile=this.getFile();
        YCasesDeturn ycase=this.getModel(YCasesDeturn.class,"casesdeturn");
        if(service.upd_YCasesDeturn(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesDeturn() {
        if(service.del_YCasesDeturn(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesDeturnAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesDeturnBycid(getParaToInt(0))));
    }
}
