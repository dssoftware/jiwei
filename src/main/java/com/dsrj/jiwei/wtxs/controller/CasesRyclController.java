package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesRycl;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesRyclService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesRyclController extends BaseController {
    @Inject
    private CasesRyclService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesRycl(){
        UploadFile uploadFile=this.getFile();
        YCasesRycl ycase=this.getModel(YCasesRycl.class,"CasesRycl");
        if(service.ins_YCasesRycl(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesRycl(){
        UploadFile uploadFile=this.getFile();
        YCasesRycl ycase=this.getModel(YCasesRycl.class,"CasesRycl");
        if(service.upd_YCasesRycl(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesRycl() {
        if(service.del_YCasesRycl(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesRyclAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesRyclBycid(getParaToInt(0))));
    }
}
