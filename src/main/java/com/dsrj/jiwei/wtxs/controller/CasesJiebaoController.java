package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesJiebao;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesJiebaoService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesJiebaoController extends BaseController {
    @Inject
    private CasesJiebaoService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesJiebao(){
        UploadFile uploadFile=this.getFile();
        YCasesJiebao ycase=this.getModel(YCasesJiebao.class,"CasesJiebao");
        if(service.ins_YCasesJiebao(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesJiebao(){
        UploadFile uploadFile=this.getFile();
        YCasesJiebao ycase=this.getModel(YCasesJiebao.class,"CasesJiebao");
        if(service.upd_YCasesJiebao(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesJiebao() {
        if(service.del_YCasesJiebao(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesJiebaoAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesJiebaoBycid(getParaToInt(0))));
    }
}
