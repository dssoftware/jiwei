package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesZuzhi;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesZuzhiService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesZuzhiController extends BaseController {
    @Inject
    private CasesZuzhiService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesZuzhi(){
        UploadFile uploadFile=this.getFile();
        YCasesZuzhi ycase=this.getModel(YCasesZuzhi.class,"CasesZuzhi");
        if(service.ins_YCasesZuzhi(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesZuzhi(){
        UploadFile uploadFile=this.getFile();
        YCasesZuzhi ycase=this.getModel(YCasesZuzhi.class,"CasesZuzhi");
        if(service.upd_YCasesZuzhi(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesZuzhi() {
        if(service.del_YCasesZuzhi(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesZuzhiAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesZuzhiBycid(getParaToInt(0))));
    }
}
