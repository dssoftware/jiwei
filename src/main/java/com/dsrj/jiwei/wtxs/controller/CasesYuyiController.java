package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesYuyi;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesYuyiService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesYuyiController extends BaseController {
    @Inject
    private CasesYuyiService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesYuyi(){
        UploadFile uploadFile=this.getFile();
        YCasesYuyi ycase=this.getModel(YCasesYuyi.class,"CasesYuyi");
        if(service.ins_YCasesYuyi(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesYuyi(){
        UploadFile uploadFile=this.getFile();
        YCasesYuyi ycase=this.getModel(YCasesYuyi.class,"CasesYuyi");
        if(service.upd_YCasesYuyi(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesYuyi() {
        if(service.del_YCasesYuyi(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesYuyiAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesYuyiBycid(getParaToInt(0))));
    }
}
