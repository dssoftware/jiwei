package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesTanhua;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesTanhuaService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesTanhuaController extends BaseController {
    @Inject
    private CasesTanhuaService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesTanhua(){
        UploadFile uploadFile=this.getFile();
        YCasesTanhua ycase=this.getModel(YCasesTanhua.class,"CasesTanhua");
        if(service.ins_YCasesTanhua(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesTanhua(){
        UploadFile uploadFile=this.getFile();
        YCasesTanhua ycase=this.getModel(YCasesTanhua.class,"CasesTanhua");
        if(service.upd_YCasesTanhua(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesTanhua() {
        if(service.del_YCasesTanhua(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesTanhuaAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesTanhuaBycid(getParaToInt(0))));
    }
}
