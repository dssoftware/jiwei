package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesChubuHc;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesChubuHcService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 15:54
 * @Version 1.0
 * @Content 创建一个java文本 问题线索初步核实核查
 */
public class CasesChubuHcController extends BaseController {
    @Inject
    private CasesChubuHcService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesChubuHc(){
        UploadFile uploadFile=this.getFile();
        YCasesChubuHc ycase=this.getModel(YCasesChubuHc.class,"caseschubuhc");
        if(service.ins_CasesChubuHc(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesChubuHc(){
        UploadFile uploadFile=this.getFile();
        YCasesChubuHc ycase=this.getModel(YCasesChubuHc.class,"caseschubuhc");
        if(service.upd_CasesChubuHc(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesChubuHc() {
        if(service.del_CasesChubuHc(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesChubuHcAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_CasesChubuHcBycid(getParaToInt(0))));
    }
}
