package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesLevel;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesLevelService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:19
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesLevelController extends BaseController {
    @Inject
    private CasesLevelService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesLevel(){
        UploadFile uploadFile=this.getFile();
        YCasesLevel ycase=this.getModel(YCasesLevel.class,"CasesLevel");
        if(service.ins_CasesLevel(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesLevel(){
        UploadFile uploadFile=this.getFile();
        YCasesLevel ycase=this.getModel(YCasesLevel.class,"CasesLevel");
        if(service.upd_CasesLevel(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesLevel() {
        if(service.del_CasesLevel(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesLevelAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_CasesLevelBycid(getParaToInt(0))));
    }
}
