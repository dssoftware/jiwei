package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesChubu;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesChubuService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:06
 * @Version 1.0
 * @Content 创建一个java文本 问题线索初步核实功能
 */
public class CasesChubuController extends BaseController {
    @Inject
    private CasesChubuService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesChubu(){
        UploadFile uploadFile=this.getFile();
        YCasesChubu ycase=this.getModel(YCasesChubu.class,"caseschubu");
        if(service.ins_CasesChubu(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesChubu(){
        UploadFile uploadFile=this.getFile();
        YCasesChubu ycase=this.getModel(YCasesChubu.class,"caseschubu");
        if(service.upd_CasesChubu(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesChubu() {
        if(service.del_CasesChubu(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesChubuAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_CasesChubuBycid(getParaToInt(0))));
    }
}
