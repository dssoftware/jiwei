package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesZancun;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesZancunService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesZancunController extends BaseController {
    @Inject
    private CasesZancunService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesZancun(){
        UploadFile uploadFile=this.getFile();
        YCasesZancun ycase=this.getModel(YCasesZancun.class,"CasesZancun");
        if(service.ins_YCasesZancun(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesZancun(){
        UploadFile uploadFile=this.getFile();
        YCasesZancun ycase=this.getModel(YCasesZancun.class,"CasesZancun");
        if(service.upd_YCasesZancun(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesZancun() {
        if(service.del_YCasesZancun(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesZancunAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesZancunBycid(getParaToInt(0))));
    }
}
