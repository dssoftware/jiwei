package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesRy;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesRyService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesRyController extends BaseController {
    @Inject
    private CasesRyService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesRy(){
        UploadFile uploadFile=this.getFile();
        YCasesRy ycase=this.getModel(YCasesRy.class,"CasesRy");
        if(service.ins_CasesRy(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesRy(){
        UploadFile uploadFile=this.getFile();
        YCasesRy ycase=this.getModel(YCasesRy.class,"CasesRy");
        if(service.upd_CasesRy(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesRy() {
        if(service.del_CasesRy(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesRyAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_CasesRyBycid(getParaToInt(0))));
    }
}
