package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesDetransfer;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesDetransferService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:09
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesDetransferController extends BaseController {
    @Inject
    private CasesDetransferService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesDetransfer(){
        UploadFile uploadFile=this.getFile();
        YCasesDetransfer ycase=this.getModel(YCasesDetransfer.class,"casesdetransfer");
        if(service.ins_YCasesDetransfer(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesDetransfer(){
        UploadFile uploadFile=this.getFile();
        YCasesDetransfer ycase=this.getModel(YCasesDetransfer.class,"casesdetransfer");
        if(service.upd_YCasesDetransfer(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesDetransfer() {
        if(service.del_YCasesDetransfer(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesDetransferAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesDetransferBycid(getParaToInt(0))));
    }
}
