package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesYisong;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesYisongService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesYisongController extends BaseController {
    @Inject
    private CasesYisongService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesYisong(){
        UploadFile uploadFile=this.getFile();
        YCasesYisong ycase=this.getModel(YCasesYisong.class,"CasesYisong");
        if(service.ins_YCasesYisong(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesYisong(){
        UploadFile uploadFile=this.getFile();
        YCasesYisong ycase=this.getModel(YCasesYisong.class,"CasesYisong");
        if(service.upd_YCasesYisong(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesYisong() {
        if(service.del_YCasesYisong(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesYisongAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesYisongBycid(getParaToInt(0))));
    }
}
