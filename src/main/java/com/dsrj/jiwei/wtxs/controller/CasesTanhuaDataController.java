package com.dsrj.jiwei.wtxs.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCasesTanhuaData;
import com.dsrj.system.controller.BaseController;
import com.dsrj.system.service.cases.CasesTanhuaDataService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 16:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesTanhuaDataController extends BaseController {
    @Inject
    private CasesTanhuaDataService service;
    //插入
    /*分类处置时 参数form表单*/
    public  void ins_YCasesTanhuaData(){
        UploadFile uploadFile=this.getFile();
        YCasesTanhuaData ycase=this.getModel(YCasesTanhuaData.class,"CasesTanhuaData");
        if(service.ins_YCasesTanhuaData(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改
    public void upd_YCasesTanhuaData(){
        UploadFile uploadFile=this.getFile();
        YCasesTanhuaData ycase=this.getModel(YCasesTanhuaData.class,"CasesTanhuaData");
        if(service.upd_YCasesTanhuaData(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //删除
    public void del_YCasesTanhuaData() {
        if(service.del_YCasesTanhuaData(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查询 cid
    public void sel_YCasesTanhuaDataAsId(){
        renderJson(new RD(ConstCode.SUCCESS,service.sel_YCasesTanhuaDataBycid(getParaToInt(0))));
    }
}
