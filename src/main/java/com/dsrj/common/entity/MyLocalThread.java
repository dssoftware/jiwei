package com.dsrj.common.entity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.kit.Kv;

/**
-----------------------------------
*Describtion:本地线程缓存 用来缓存request 与 response ，用于全局获取使用
*-----------------------------------
*Creatime:2019年3月18日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment: 配置线程缓存实体类-》配置全局拦截器，在请求访问是缓存request 和response -》Application中配置启动对应的全局拦截器
*
*tip:在执行了ThreadLocal.set()方法之后一定要记得使用ThreadLocal.remove(),将不要的数据移除掉，避免内存泄漏
*-----------------------------------
**/
public class MyLocalThread {

	private static ThreadLocal<Kv> threadLocal = new ThreadLocal<Kv>();
	public static final String REQUEST_KEY = "request_key";
	public static final String RESPONSE_KEY = "response_key";
	public static final String TOKEN_KEY = "token_key";
	public static final String ROLEANDPOWER_KEY = "roleandpower_key";
	public static final String USERID_KEY = "userid_key";
	public static void setThreadLocal(Kv kv){
		threadLocal.set(kv);
	}
	
	public static Kv getThreadLocal(){
		return threadLocal.get();
	}
	
	/**
	 * 获取request
	 * @return
	 */
	public static HttpServletRequest getRequest(){
		return (HttpServletRequest) getThreadLocal().get(REQUEST_KEY);
	}
	/**
	 * 获取response
	 * @return
	 */
	public static HttpServletResponse getResopnse(){
		return (HttpServletResponse) getThreadLocal().get(RESPONSE_KEY);
	}
	
	/**
	 * 获取Token
	 * @return
	 */
	public static String getToken(){
		return (String)getThreadLocal().get(USERID_KEY);
	}
	
	/**
	 * 获取用户id
	 * @return
	 */
	public static String getUserId(){
		return (String)getThreadLocal().get(TOKEN_KEY);
	}
	 
	
	
}
