package com.dsrj.common.entity;

import java.util.List;
/**
 * -----------------------------------
 * Describtion:分页查询返回的结果
 * -----------------------------------
 * Creatime:2020年01月11日
 * -----------------------------------
 * Author:mouzebin
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class PageResult {
    /**
     * 总页数
     */
    private Integer totalPage;

    /**
     * 总记录数
     */
    private Integer thtalRow;

    /**
     * 当前页结果
     */
    private List rows;

    public PageResult(Integer totalPage, Integer thtalRow, List rows) {
        this.totalPage = totalPage;
        this.thtalRow = thtalRow;
        this.rows = rows;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getThtalRow() {
        return thtalRow;
    }

    public void setThtalRow(Integer thtalRow) {
        this.thtalRow = thtalRow;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
