package com.dsrj.common.entity;

/**
 * -----------------------------------
 * Describtion:分页查询接受的条件
 * -----------------------------------
 * Creatime:2020年01月11日
 * -----------------------------------
 * Author:mouzebin
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class Paginate {

    /**
     * 页码
     */
    private Integer currentPage;

    /**
     * 每页显示条数
     */
    private Integer pageSize;

    /**
     * 查询条件
     */
    private String queryString;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}
