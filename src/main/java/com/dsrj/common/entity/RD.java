package com.dsrj.common.entity;
/**
-----------------------------------
*Describtion:返回前端数据对象
*-----------------------------------
*Creatime:2020年1月9日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
/**
 * @author Administrator
 *
 */
public class RD {

	
	/**
	 *操作状态码
	 */
	private int code ;
	
	/**
	 *操作结果是否 正常情况 
	 */
	private boolean status ;
	
	/**
	 *操作提示 
	 */
	private String msg;
	
	/**
	 * 返回数据
	 */
	private Object data;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 返回结果带 实际数据
	 * @param code
	 * @param status
	 * @param msg
	 * @param data
	 */
	public RD(int code, boolean status, String msg, Object data) {
		super();
		this.code = code;
		this.status = status;
		this.msg = msg;
		this.data = data;
	}
	
	/**
	 * 返回结果带 实际数据
	 * @param code
	 * @param status
	 * @param msg
	 * @param data
	 */
	public RD(ConstCode constcode, Object data) {
		super();
		this.code = constcode.getCode();
		this.status = constcode.getStatus();
		this.msg = constcode.getMsg();
		this.data = data;
	}
	
	/**
	 * 返回结果不带实际数据
	 * @param code
	 * @param status
	 * @param msg
	 * @param data
	 */
	public RD(ConstCode constcode) {
		super();
		this.code = constcode.getCode();
		this.status = constcode.getStatus();
		this.msg = constcode.getMsg();
	}
	
	/**
	 * 
	 * 返回结果不带 实际数据
	 * @param code
	 * @param status
	 * @param msg
	 */
	public RD(int code, boolean status, String msg) {
		super();
		this.code = code;
		this.status = status;
		this.msg = msg;
	}

	public RD() {
		super();
	}
	
	

	

	
}
