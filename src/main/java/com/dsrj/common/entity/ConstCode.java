package com.dsrj.common.entity;
/**
-----------------------------------
*Describtion:常量
*-----------------------------------
*Creatime:2019年3月5日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public enum ConstCode {
	SUCCESS(200,true,"成功"),
    NODATA(404,true,"查询成功无记录"),
    FEAILED(500,false,"失败"),
    DO_SUCCESS(0003,true,"操作成功"),
    DO_ERROR(0004,false,"操作失败"),
    ACCOUNT_ILLEGAL(1000,false, "非法访问"),
    API_NOT_EXISTS(1001,false, "请求的接口不存在"),
    API_NOT_PER(1002,false, "没有该接口的访问权限"),
    PARAMS_ERROR(1004,false, "参数为空或格式错误"),
    SIGN_ERROR(1005,false, "数据签名错误"),
    API_DISABLE(1011,false, "查询权限已被限制"),
    UNKNOWN_IP(1099,false, "非法IP请求"),
    SYSTEM_ERROR(9999,false, "系统异常"),
	LOGIN_SUCCESS(2001,true,"登录成功"),
	LOGIN_ERROR(2002,false,"账户或密码错误"),
	LOGIN_OVERTIME(2003,false,"登录超时"),
	LOGIN_OUT(2005,true,"退出登录"),
	TOKEN_OVERTIME(2006,false,"token过期"),
	TOKEN_ILLEGAL(2007,false,"token非法"),
	RANDOMCODE_ERROR(2004,false,"验证码错误"),
	RENDER_404_ERROR(404,false,"请求不存在"),
	RENDER_500_ERROR(500,false,"请求处理异常");
	

	/**
	 *状态 码
	 */
	private int code;
	

	/**
	 *操作是否成功
	 */
	private Boolean status;
	

	/**
	 *操作名称 
	 */
	private String msg;
	

	/**
	 *返回数据 
	 */
	private Object data;
	
	public int getCode() {
		return code;
	}
	public String getMsg() {
		return msg;
	}
	 public Boolean getStatus() {
		return status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	private ConstCode(int code,Boolean status, String msg) {
	        this.code = code;
	        this.status=status;
	        this.msg = msg;
	}
	
	
}
