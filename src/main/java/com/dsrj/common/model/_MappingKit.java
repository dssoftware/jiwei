package com.dsrj.common.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("sys_job_manager", "id", SysJobManager.class);
		arp.addMapping("sys_log", "id", SysLog.class);
		arp.addMapping("sys_menu", "id", SysMenu.class);
		arp.addMapping("sys_role", "id", SysRole.class);
		arp.addMapping("sys_role_menu", "id", SysRoleMenu.class);
		arp.addMapping("sys_user", "id", SysUser.class);
		arp.addMapping("sys_user_role", "id", SysUserRole.class);
		arp.addMapping("y_cases", "id", YCases.class);
		arp.addMapping("y_cases_chubu", "id", YCasesChubu.class);
		arp.addMapping("y_cases_chubu_hc", "id", YCasesChubuHc.class);
		arp.addMapping("y_cases_detransfer", "id", YCasesDetransfer.class);
		arp.addMapping("y_cases_deturn", "id", YCasesDeturn.class);
		arp.addMapping("y_cases_jiebao", "id", YCasesJiebao.class);
		arp.addMapping("y_cases_level", "id", YCasesLevel.class);
		arp.addMapping("y_cases_ry", "id", YCasesRy.class);
		arp.addMapping("y_cases_rycl", "id", YCasesRycl.class);
		arp.addMapping("y_cases_tanhua", "id", YCasesTanhua.class);
		arp.addMapping("y_cases_tanhua_data", "id", YCasesTanhuaData.class);
		arp.addMapping("y_cases_yisong", "id", YCasesYisong.class);
		arp.addMapping("y_cases_yuyi", "id", YCasesYuyi.class);
		arp.addMapping("y_cases_zancun", "id", YCasesZancun.class);
		arp.addMapping("y_cases_zuzhi", "id", YCasesZuzhi.class);
		arp.addMapping("y_law", "id", YLaw.class);
		arp.addMapping("y_trial", "id", YTrial.class);
		arp.addMapping("y_trial_cases", "id", YTrialCases.class);
		arp.addMapping("y_trial_chufen", "id", YTrialChufen.class);
		arp.addMapping("y_trial_chufen_shensu", "id", YTrialChufenShensu.class);
		arp.addMapping("y_trial_cuoshi", "id", YTrialCuoshi.class);
		arp.addMapping("y_trial_jieguo", "id", YTrialJieguo.class);
		arp.addMapping("y_trial_level", "id", YTrialLevel.class);
		arp.addMapping("y_trial_ry", "id", YTrialRy.class);
		arp.addMapping("y_trial_shean", "id", YTrialShean.class);
		arp.addMapping("y_trial_shenli", "id", YTrialShenli.class);
		arp.addMapping("y_trial_shenli_huiyi_bumen", "id", YTrialShenliHuiyiBumen.class);
		arp.addMapping("y_trial_shenli_huiyi_zu", "id", YTrialShenliHuiyiZu.class);
		arp.addMapping("y_trial_shenli_jieru", "id", YTrialShenliJieru.class);
		arp.addMapping("y_trial_shenli_sifa", "id", YTrialShenliSifa.class);
		arp.addMapping("y_trial_shenli_weiji", "id", YTrialShenliWeiji.class);
		arp.addMapping("y_trial_zhixing", "id", YTrialZhixing.class);
		arp.addMapping("y_trial_zhuanti", "id", YTrialZhuanti.class);
		arp.addMapping("y_xuser", "id", YXuser.class);
	}
}

