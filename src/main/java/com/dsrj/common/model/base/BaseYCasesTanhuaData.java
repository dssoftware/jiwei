package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYCasesTanhuaData<M extends BaseYCasesTanhuaData<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setType(java.lang.String type) {
		set("type", type);
	}
	
	public java.lang.String getType() {
		return getStr("type");
	}

	public void setCid(java.lang.Long cid) {
		set("cid", cid);
	}
	
	public java.lang.Long getCid() {
		return getLong("cid");
	}

	public void setRoleid(java.lang.Boolean roleid) {
		set("roleid", roleid);
	}
	
	public java.lang.Boolean getRoleid() {
		return get("roleid");
	}

	public void setFileurl(java.lang.String fileurl) {
		set("fileurl", fileurl);
	}
	
	public java.lang.String getFileurl() {
		return getStr("fileurl");
	}

	public void setZiliaofiles(java.lang.String ziliaofiles) {
		set("ziliaofiles", ziliaofiles);
	}
	
	public java.lang.String getZiliaofiles() {
		return getStr("ziliaofiles");
	}

	public void setContent(java.lang.String content) {
		set("content", content);
	}
	
	public java.lang.String getContent() {
		return getStr("content");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

	public void setUpdateTime(java.lang.Long updateTime) {
		set("update_time", updateTime);
	}
	
	public java.lang.Long getUpdateTime() {
		return getLong("update_time");
	}

}
