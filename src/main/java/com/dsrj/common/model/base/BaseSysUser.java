package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSysUser<M extends BaseSysUser<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public void setUsername(java.lang.String username) {
		set("username", username);
	}
	
	public java.lang.String getUsername() {
		return getStr("username");
	}

	public void setPassword(java.lang.String password) {
		set("password", password);
	}
	
	public java.lang.String getPassword() {
		return getStr("password");
	}

	public void setPhone(java.lang.String phone) {
		set("phone", phone);
	}
	
	public java.lang.String getPhone() {
		return getStr("phone");
	}

	public void setAddress(java.lang.String address) {
		set("address", address);
	}
	
	public java.lang.String getAddress() {
		return getStr("address");
	}

	public void setEmail(java.lang.String email) {
		set("email", email);
	}
	
	public java.lang.String getEmail() {
		return getStr("email");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public void setSex(java.lang.Integer sex) {
		set("sex", sex);
	}
	
	public java.lang.Integer getSex() {
		return getInt("sex");
	}

	public void setBirthday(java.lang.String birthday) {
		set("birthday", birthday);
	}
	
	public java.lang.String getBirthday() {
		return getStr("birthday");
	}

	public void setOldpassword(java.lang.String oldpassword) {
		set("oldpassword", oldpassword);
	}
	
	public java.lang.String getOldpassword() {
		return getStr("oldpassword");
	}

	public void setIp(java.lang.String ip) {
		set("ip", ip);
	}
	
	public java.lang.String getIp() {
		return getStr("ip");
	}

	public void setRoles(java.lang.String roles) {
		set("roles", roles);
	}
	
	public java.lang.String getRoles() {
		return getStr("roles");
	}

	public void setCreateman(java.lang.String createman) {
		set("createman", createman);
	}
	
	public java.lang.String getCreateman() {
		return getStr("createman");
	}

	public void setCreatedate(java.lang.String createdate) {
		set("createdate", createdate);
	}
	
	public java.lang.String getCreatedate() {
		return getStr("createdate");
	}

	public void setEnable(java.lang.Integer enable) {
		set("enable", enable);
	}
	
	public java.lang.Integer getEnable() {
		return getInt("enable");
	}

}
