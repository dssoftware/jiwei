package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYTrialRy<M extends BaseYTrialRy<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setLevel(java.lang.Boolean level) {
		set("level", level);
	}
	
	public java.lang.Boolean getLevel() {
		return get("level");
	}

	public void setCid(java.lang.Long cid) {
		set("cid", cid);
	}
	
	public java.lang.Long getCid() {
		return getLong("cid");
	}

	public void setTableid(java.lang.Long tableid) {
		set("tableid", tableid);
	}
	
	public java.lang.Long getTableid() {
		return getLong("tableid");
	}

	public void setUid(java.lang.Long uid) {
		set("uid", uid);
	}
	
	public java.lang.Long getUid() {
		return getLong("uid");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

}
