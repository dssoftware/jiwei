package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYLaw<M extends BaseYLaw<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public void setTiaokuan(java.lang.String tiaokuan) {
		set("tiaokuan", tiaokuan);
	}
	
	public java.lang.String getTiaokuan() {
		return getStr("tiaokuan");
	}

	public void setLimits(java.lang.String limits) {
		set("limits", limits);
	}
	
	public java.lang.String getLimits() {
		return getStr("limits");
	}

	public void setKuan(java.lang.String kuan) {
		set("kuan", kuan);
	}
	
	public java.lang.String getKuan() {
		return getStr("kuan");
	}

	public void setXiang(java.lang.String xiang) {
		set("xiang", xiang);
	}
	
	public java.lang.String getXiang() {
		return getStr("xiang");
	}

	public void setContent(java.lang.String content) {
		set("content", content);
	}
	
	public java.lang.String getContent() {
		return getStr("content");
	}

	public void setSort(java.lang.Integer sort) {
		set("sort", sort);
	}
	
	public java.lang.Integer getSort() {
		return getInt("sort");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

	public void setUpdateTime(java.lang.Long updateTime) {
		set("update_time", updateTime);
	}
	
	public java.lang.Long getUpdateTime() {
		return getLong("update_time");
	}

}
