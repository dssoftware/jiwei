package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYTrialCuoshi<M extends BaseYTrialCuoshi<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setRoleid(java.lang.Boolean roleid) {
		set("roleid", roleid);
	}
	
	public java.lang.Boolean getRoleid() {
		return get("roleid");
	}

	public void setStatus(java.lang.Boolean status) {
		set("status", status);
	}
	
	public java.lang.Boolean getStatus() {
		return get("status");
	}

	public void setTid(java.lang.Long tid) {
		set("tid", tid);
	}
	
	public java.lang.Long getTid() {
		return getLong("tid");
	}

	public void setSczl(java.lang.String sczl) {
		set("sczl", sczl);
	}
	
	public java.lang.String getSczl() {
		return getStr("sczl");
	}

	public void setSsdx(java.lang.String ssdx) {
		set("ssdx", ssdx);
	}
	
	public java.lang.String getSsdx() {
		return getStr("ssdx");
	}

	public void setPzbm(java.lang.String pzbm) {
		set("pzbm", pzbm);
	}
	
	public java.lang.String getPzbm() {
		return getStr("pzbm");
	}

	public void setPzrq(java.lang.String pzrq) {
		set("pzrq", pzrq);
	}
	
	public java.lang.String getPzrq() {
		return getStr("pzrq");
	}

	public void setJsrq(java.lang.String jsrq) {
		set("jsrq", jsrq);
	}
	
	public java.lang.String getJsrq() {
		return getStr("jsrq");
	}

	public void setZxjg(java.lang.String zxjg) {
		set("zxjg", zxjg);
	}
	
	public java.lang.String getZxjg() {
		return getStr("zxjg");
	}

	public void setDyws(java.lang.String dyws) {
		set("dyws", dyws);
	}
	
	public java.lang.String getDyws() {
		return getStr("dyws");
	}

	public void setWsbh(java.lang.String wsbh) {
		set("wsbh", wsbh);
	}
	
	public java.lang.String getWsbh() {
		return getStr("wsbh");
	}

	public void setSawp(java.lang.String sawp) {
		set("sawp", sawp);
	}
	
	public java.lang.String getSawp() {
		return getStr("sawp");
	}

	public void setNote(java.lang.String note) {
		set("note", note);
	}
	
	public java.lang.String getNote() {
		return getStr("note");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

	public void setUpdateTime(java.lang.Long updateTime) {
		set("update_time", updateTime);
	}
	
	public java.lang.Long getUpdateTime() {
		return getLong("update_time");
	}

}
