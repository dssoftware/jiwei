package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYCasesChubu<M extends BaseYCasesChubu<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setCid(java.lang.Long cid) {
		set("cid", cid);
	}
	
	public java.lang.Long getCid() {
		return getLong("cid");
	}

	public void setRoleid(java.lang.Boolean roleid) {
		set("roleid", roleid);
	}
	
	public java.lang.Boolean getRoleid() {
		return get("roleid");
	}

	public void setLevel(java.lang.String level) {
		set("level", level);
	}
	
	public java.lang.String getLevel() {
		return getStr("level");
	}

	public void setYes(java.lang.Boolean yes) {
		set("yes", yes);
	}
	
	public java.lang.Boolean getYes() {
		return get("yes");
	}

	public void setBh(java.lang.String bh) {
		set("bh", bh);
	}
	
	public java.lang.String getBh() {
		return getStr("bh");
	}

	public void setChCzyj(java.lang.String chCzyj) {
		set("ch_czyj", chCzyj);
	}
	
	public java.lang.String getChCzyj() {
		return getStr("ch_czyj");
	}

	public void setChczjy(java.lang.String chczjy) {
		set("chczjy", chczjy);
	}
	
	public java.lang.String getChczjy() {
		return getStr("chczjy");
	}

	public void setJbry(java.lang.String jbry) {
		set("jbry", jbry);
	}
	
	public java.lang.String getJbry() {
		return getStr("jbry");
	}

	public void setJybprq(java.lang.String jybprq) {
		set("jybprq", jybprq);
	}
	
	public java.lang.String getJybprq() {
		return getStr("jybprq");
	}

	public void setCprq(java.lang.String cprq) {
		set("cprq", cprq);
	}
	
	public java.lang.String getCprq() {
		return getStr("cprq");
	}

	public void setChczyj(java.lang.String chczyj) {
		set("chczyj", chczyj);
	}
	
	public java.lang.String getChczyj() {
		return getStr("chczyj");
	}

	public void setYjsprq(java.lang.String yjsprq) {
		set("yjsprq", yjsprq);
	}
	
	public java.lang.String getYjsprq() {
		return getStr("yjsprq");
	}

	public void setCzspyj(java.lang.String czspyj) {
		set("czspyj", czspyj);
	}
	
	public java.lang.String getCzspyj() {
		return getStr("czspyj");
	}

	public void setChBprq(java.lang.String chBprq) {
		set("ch_bprq", chBprq);
	}
	
	public java.lang.String getChBprq() {
		return getStr("ch_bprq");
	}

	public void setChSprq(java.lang.String chSprq) {
		set("ch_sprq", chSprq);
	}
	
	public java.lang.String getChSprq() {
		return getStr("ch_sprq");
	}

	public void setChSpr(java.lang.String chSpr) {
		set("ch_spr", chSpr);
	}
	
	public java.lang.String getChSpr() {
		return getStr("ch_spr");
	}

	public void setCbbm(java.lang.String cbbm) {
		set("cbbm", cbbm);
	}
	
	public java.lang.String getCbbm() {
		return getStr("cbbm");
	}

	public void setHczcy(java.lang.String hczcy) {
		set("hczcy", hczcy);
	}
	
	public java.lang.String getHczcy() {
		return getStr("hczcy");
	}

	public void setChBgbz(java.lang.String chBgbz) {
		set("ch_bgbz", chBgbz);
	}
	
	public java.lang.String getChBgbz() {
		return getStr("ch_bgbz");
	}

	public void setChKsrq(java.lang.String chKsrq) {
		set("ch_ksrq", chKsrq);
	}
	
	public java.lang.String getChKsrq() {
		return getStr("ch_ksrq");
	}

	public void setChZzrq(java.lang.String chZzrq) {
		set("ch_zzrq", chZzrq);
	}
	
	public java.lang.String getChZzrq() {
		return getStr("ch_zzrq");
	}

	public void setChYcsx1(java.lang.String chYcsx1) {
		set("ch_ycsx1", chYcsx1);
	}
	
	public java.lang.String getChYcsx1() {
		return getStr("ch_ycsx1");
	}

	public void setChYzpzrq1(java.lang.String chYzpzrq1) {
		set("ch_yzpzrq1", chYzpzrq1);
	}
	
	public java.lang.String getChYzpzrq1() {
		return getStr("ch_yzpzrq1");
	}

	public void setChYcsx2(java.lang.String chYcsx2) {
		set("ch_ycsx2", chYcsx2);
	}
	
	public java.lang.String getChYcsx2() {
		return getStr("ch_ycsx2");
	}

	public void setChYzpzrq2(java.lang.String chYzpzrq2) {
		set("ch_yzpzrq2", chYzpzrq2);
	}
	
	public java.lang.String getChYzpzrq2() {
		return getStr("ch_yzpzrq2");
	}

	public void setChHcdcz(java.lang.String chHcdcz) {
		set("ch_hcdcz", chHcdcz);
	}
	
	public java.lang.String getChHcdcz() {
		return getStr("ch_hcdcz");
	}

	public void setChHcdcz1(java.lang.String chHcdcz1) {
		set("ch_hcdcz1", chHcdcz1);
	}
	
	public java.lang.String getChHcdcz1() {
		return getStr("ch_hcdcz1");
	}

	public void setChHcdcz2(java.lang.String chHcdcz2) {
		set("ch_hcdcz2", chHcdcz2);
	}
	
	public java.lang.String getChHcdcz2() {
		return getStr("ch_hcdcz2");
	}

	public void setChHcdcz3(java.lang.String chHcdcz3) {
		set("ch_hcdcz3", chHcdcz3);
	}
	
	public java.lang.String getChHcdcz3() {
		return getStr("ch_hcdcz3");
	}

	public void setChHcdcz4(java.lang.String chHcdcz4) {
		set("ch_hcdcz4", chHcdcz4);
	}
	
	public java.lang.String getChHcdcz4() {
		return getStr("ch_hcdcz4");
	}

	public void setChCbbm(java.lang.String chCbbm) {
		set("ch_cbbm", chCbbm);
	}
	
	public java.lang.String getChCbbm() {
		return getStr("ch_cbbm");
	}

	public void setChCbbm1(java.lang.String chCbbm1) {
		set("ch_cbbm1", chCbbm1);
	}
	
	public java.lang.String getChCbbm1() {
		return getStr("ch_cbbm1");
	}

	public void setChCbbm2(java.lang.String chCbbm2) {
		set("ch_cbbm2", chCbbm2);
	}
	
	public java.lang.String getChCbbm2() {
		return getStr("ch_cbbm2");
	}

	public void setChCbbm3(java.lang.String chCbbm3) {
		set("ch_cbbm3", chCbbm3);
	}
	
	public java.lang.String getChCbbm3() {
		return getStr("ch_cbbm3");
	}

	public void setChCbbm4(java.lang.String chCbbm4) {
		set("ch_cbbm4", chCbbm4);
	}
	
	public java.lang.String getChCbbm4() {
		return getStr("ch_cbbm4");
	}

	public void setGzTlrq(java.lang.String gzTlrq) {
		set("gz_tlrq", gzTlrq);
	}
	
	public java.lang.String getGzTlrq() {
		return getStr("gz_tlrq");
	}

	public void setGzCprq(java.lang.String gzCprq) {
		set("gz_cprq", gzCprq);
	}
	
	public java.lang.String getGzCprq() {
		return getStr("gz_cprq");
	}

	public void setGzPzrq(java.lang.String gzPzrq) {
		set("gz_pzrq", gzPzrq);
	}
	
	public java.lang.String getGzPzrq() {
		return getStr("gz_pzrq");
	}

	public void setGzFabz(java.lang.String gzFabz) {
		set("gz_fabz", gzFabz);
	}
	
	public java.lang.String getGzFabz() {
		return getStr("gz_fabz");
	}

	public void setGzTljl(java.lang.String gzTljl) {
		set("gz_tljl", gzTljl);
	}
	
	public java.lang.String getGzTljl() {
		return getStr("gz_tljl");
	}

	public void setYsdzz(java.lang.String ysdzz) {
		set("ysdzz", ysdzz);
	}
	
	public java.lang.String getYsdzz() {
		return getStr("ysdzz");
	}

	public void setYsdzzrq(java.lang.String ysdzzrq) {
		set("ysdzzrq", ysdzzrq);
	}
	
	public java.lang.String getYsdzzrq() {
		return getStr("ysdzzrq");
	}

	public void setYsbm(java.lang.String ysbm) {
		set("ysbm", ysbm);
	}
	
	public java.lang.String getYsbm() {
		return getStr("ysbm");
	}

	public void setYsbmrq(java.lang.String ysbmrq) {
		set("ysbmrq", ysbmrq);
	}
	
	public java.lang.String getYsbmrq() {
		return getStr("ysbmrq");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

	public void setUpdateTime(java.lang.Long updateTime) {
		set("update_time", updateTime);
	}
	
	public java.lang.Long getUpdateTime() {
		return getLong("update_time");
	}

}
