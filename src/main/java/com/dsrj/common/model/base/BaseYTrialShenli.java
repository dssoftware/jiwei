package com.dsrj.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseYTrialShenli<M extends BaseYTrialShenli<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setTid(java.lang.Long tid) {
		set("tid", tid);
	}
	
	public java.lang.Long getTid() {
		return getLong("tid");
	}

	public void setRoleid(java.lang.Boolean roleid) {
		set("roleid", roleid);
	}
	
	public java.lang.Boolean getRoleid() {
		return get("roleid");
	}

	public void setYes(java.lang.Boolean yes) {
		set("yes", yes);
	}
	
	public java.lang.Boolean getYes() {
		return get("yes");
	}

	public void setAnhao(java.lang.String anhao) {
		set("anhao", anhao);
	}
	
	public java.lang.String getAnhao() {
		return getStr("anhao");
	}

	public void setAAjslbm(java.lang.String aAjslbm) {
		set("a_ajslbm", aAjslbm);
	}
	
	public java.lang.String getAAjslbm() {
		return getStr("a_ajslbm");
	}

	public void setAJssj(java.lang.String aJssj) {
		set("a_jssj", aJssj);
	}
	
	public java.lang.String getAJssj() {
		return getStr("a_jssj");
	}

	public void setAJsr(java.lang.String aJsr) {
		set("a_jsr", aJsr);
	}
	
	public java.lang.String getAJsr() {
		return getStr("a_jsr");
	}

	public void setAXsscrq(java.lang.String aXsscrq) {
		set("a_xsscrq", aXsscrq);
	}
	
	public java.lang.String getAXsscrq() {
		return getStr("a_xsscrq");
	}

	public void setAScr(java.lang.String aScr) {
		set("a_scr", aScr);
	}
	
	public java.lang.String getAScr() {
		return getStr("a_scr");
	}

	public void setASccbyj(java.lang.String aSccbyj) {
		set("a_sccbyj", aSccbyj);
	}
	
	public java.lang.String getASccbyj() {
		return getStr("a_sccbyj");
	}

	public void setAZsslrq(java.lang.String aZsslrq) {
		set("a_zsslrq", aZsslrq);
	}
	
	public java.lang.String getAZsslrq() {
		return getStr("a_zsslrq");
	}

	public void setATqjr(java.lang.String aTqjr) {
		set("a_tqjr", aTqjr);
	}
	
	public java.lang.String getATqjr() {
		return getStr("a_tqjr");
	}

	public void setYQs(java.lang.String yQs) {
		set("y_qs", yQs);
	}
	
	public java.lang.String getYQs() {
		return getStr("y_qs");
	}

	public void setAXingshi(java.lang.String aXingshi) {
		set("a_xingshi", aXingshi);
	}
	
	public java.lang.String getAXingshi() {
		return getStr("a_xingshi");
	}

	public void setShencha(java.lang.String shencha) {
		set("shencha", shencha);
	}
	
	public java.lang.String getShencha() {
		return getStr("shencha");
	}

	public void setShenli(java.lang.String shenli) {
		set("shenli", shenli);
	}
	
	public java.lang.String getShenli() {
		return getStr("shenli");
	}

	public void setTanhua(java.lang.String tanhua) {
		set("tanhua", tanhua);
	}
	
	public java.lang.String getTanhua() {
		return getStr("tanhua");
	}

	public void setBaogao(java.lang.String baogao) {
		set("baogao", baogao);
	}
	
	public java.lang.String getBaogao() {
		return getStr("baogao");
	}

	public void setCreateTime(java.lang.Long createTime) {
		set("create_time", createTime);
	}
	
	public java.lang.Long getCreateTime() {
		return getLong("create_time");
	}

	public void setUpdateTime(java.lang.Long updateTime) {
		set("update_time", updateTime);
	}
	
	public java.lang.Long getUpdateTime() {
		return getLong("update_time");
	}

}
