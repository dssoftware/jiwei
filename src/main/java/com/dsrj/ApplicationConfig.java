package com.dsrj;

import com.dsrj.common.model._MappingKit;
import com.dsrj.system.routes.AdminRoutes;
import com.dsrj.system.routes.FrontRoutes;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import com.mysql.jdbc.Connection;

/**
 * 
 * API 引导式配置
 */
public class ApplicationConfig extends JFinalConfig {
	
	/**
	 * 启动入口，运行此 main 方法可以启动项目，此 main 方法可以放置在任意的 Class 类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		UndertowServer.start(ApplicationConfig.class,80,true);
	}
	
	static Prop p;
	
	public static FrontRoutes frontRoutes;
	
	public static DruidPlugin createDruidPlugin() {
		loadConfig();
		
		return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
	}
	
	/**
	 * 先加载开发环境配置，然后尝试加载生产环境配置，生产环境配置不存在时不会抛异常
	 * 在生产环境部署时后动创建 dbconfig-pro.txt，添加的配置项可以覆盖掉
	 * dbconfig.txt 中的配置项
	 */
	static void loadConfig() {
		if (p == null) {
			p = PropKit.use("dbconfig.txt").appendIfExists("dbconfig-pro.txt");
		}
	}
	
	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		loadConfig();
		
		me.setDevMode(p.getBoolean("devMode", false));
		
		// 支持 Controller、Interceptor 之中使用 @Inject 注入业务层，并且自动实现 AOP
		me.setInjectDependency(true);

	}
	
	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add(new AdminRoutes());
		//前端交互路由
		frontRoutes =new FrontRoutes();
		me.add(frontRoutes);
		//后端路由交互
	}
	
	/*
	 * 模板引擎  ，本套开发 无特殊情况不使用
	 * */
	@Override
	public void configEngine(Engine me) {
		me.setDevMode(p.getBoolean("engineMode", false));
		me.addSharedFunction("/common/_layout.html");
		me.addSharedFunction("/common/_paginate.html");
	}
	
	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 配置 druid 数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
		me.add(druidPlugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
		// 所有映射在 MappingKit 中自动化搞定
		arp.setShowSql(p.getBoolean("devMode", false));
//		arp.setBaseSqlTemplatePath(PathKit.getWebRootPath()+"/sql");
//		arp.addSqlTemplate("all.sql");
		_MappingKit.mapping(arp);
		me.add(arp);
	}
	
	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
	}
	
	/**
	 * 配置处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		
	}
}
