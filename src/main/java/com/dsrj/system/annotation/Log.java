package com.dsrj.system.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日志注解注解（功能权限）   
 * 
 * @author MileZL
 * 
 * @date 2020-01-08 11:24
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
    //校验的参数
    String value();
}
