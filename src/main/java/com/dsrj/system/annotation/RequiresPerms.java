package com.dsrj.system.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能权限注解（功能权限）   
 * 
 * tip：权限等级分为 功能权限RequiresPerms  角色权限RequiresRoles  用户权限RequiresAdmin 数据权限RequiresData
 * @author mzb
 * @date 2020-01-08 11:24
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresPerms {
    //校验的参数
    String value();
}
