package com.dsrj.system.routes;


import com.dsrj.system.interceptor.LogInterceptor;
import com.dsrj.system.interceptor.LoginInterceptor;
import com.dsrj.system.login.LoginController;
import com.jfinal.config.Routes;

/**
-----------------------------------
*Describtion:前端接口交互路由。
*-----------------------------------
*Creatime:2020年1月8日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:前端路由 拦截发生页面调整时的请求和无需验证的请求 ：例如登录、静态资源获取
*-----------------------------------
**/
public class FrontRoutes extends Routes{
    @Override
    public void config() {
       //全局拦截
//        this.addInterceptor(new GlobolInterceptor());
        //登录拦截
        this.addInterceptor(new LoginInterceptor());
        //权限验证
//        this.addInterceptor(new AnnotationParaInterceptor());
        //记录日志
//       this.addInterceptor(new LogInterceptor());
       
        this.add("/login", LoginController.class);
    }

}
