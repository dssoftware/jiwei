package com.dsrj.system.routes;

import com.dsrj.system.controller.*;
import com.dsrj.system.interceptor.AuthInterceptor;
import com.dsrj.system.interceptor.ExceInterceptor;
import com.dsrj.system.interceptor.GlobolInterceptor;
import com.dsrj.system.interceptor.LogInterceptor;
import com.dsrj.system.interceptor.LoginInterceptor;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
-----------------------------------
*Describtion:后端路由
*-----------------------------------
*Creatime:2020年1月16日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment: 管理  前端直接请求后端接口的请求
*-----------------------------------
**/
public class AdminRoutes extends Routes {

	@Override
	public void config() {
		 //全局拦截
        this.addInterceptor(new GlobolInterceptor());
		//身份验证 拦截
        this.addInterceptor(new AuthInterceptor());
        //事务管理
        this.addInterceptor(new Tx());
        //异常处理
        this.addInterceptor(new ExceInterceptor());
        
        //日志处理
        this.addInterceptor(new LogInterceptor());
        
        this.add("/menu", MenuController.class);
        this.add("/index", IndexController.class);
        this.add("/user",UserController.class);
        this.add("/beifen", UtilController.class);
        this.add("/manager", JobmanageController.class);
        this.add("/law", LawController.class);
        this.add("/role",RoleController.class);
        this.add("/rolemenu",RoleMenuController.class);
        this.add("/xuser",XuserController.class);
	}

}
