package com.dsrj.system.service;

import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.SysRoleMenu;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 15:54
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class RoleMenuService {
    //增加创建关联
    public boolean ins_RoleMenu(SysRoleMenu role) {
        return role.save();
    }
    //修改关联
    public boolean upd_RoleMenu(SysRoleMenu role) {
        return role.update();
    }
    //删除关联
    public boolean del_RoleMenu(Integer id) {
        return Db.deleteById("sys_role_menu",id);
    }
    //查询关联
    public List<Record> sel_AllRoleMenu(){
        return Db.find("select * from sys_role_menu ");
    }
    //根据角色id查关联
    public List<Record> sel_RoleMenuBid(Integer id) {
        return Db.find("select * from sys_role_menu where role_id = "+id);
    }
    //分页查询
    public Page<Record> sel_RoleMenu(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  sys_role_menu where 1=1 order by id desc");
    }
}
