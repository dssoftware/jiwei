package com.dsrj.system.service;


import com.dsrj.common.entity.PageResult;
import com.dsrj.common.entity.Paginate;
import com.dsrj.common.model.SysUser;
import com.jfinal.plugin.activerecord.Page;

import java.time.LocalDateTime;

/**
 * -----------------------------------
 * Describtion:系统用户操作
 * -----------------------------------
 * Creatime:2020年01月11日
 * -----------------------------------
 * Author:mouzebin
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class UserService {

    private SysUser sysUser = new SysUser().dao();

    /**
     * 新增用户
     *
     * @param user 用户基本信息
     */
    public void add(SysUser user) {
        if (null == user) {
            //用户信息为空
            throw new RuntimeException("请输入基本信息");
        }
        String username = user.getUsername();
        if (null == username || "".equals(username)) {
            //用户未输入用户名
            throw new RuntimeException("用户名不能为空");
        }
        String password = user.getPassword();
        if (null == password || "".equals(password)) {
            //未输入密码
            throw new RuntimeException("密码不能为空");
        }
        //获取当前登录用户id
        //设定创建时间
        LocalDateTime createTime = LocalDateTime.now();
        user.setCreatedate(createTime.toString());
        //设定用户启用状态，默认启用
        user.setEnable(1);
        //将数据存储到数据库中
        try {
            user.save();
        } catch (Exception e) {
            //执行出错则返回信息
            e.printStackTrace();
            throw new RuntimeException("网络异常，请重试");
        }
    }

    /**
     * 删除用户
     *
     * @param id 用户id
     */
    public void delUser(String id) {
        if (null == id || "".equals(id)) {
            //用户id为空
            throw new RuntimeException("网络异常,请稍后重试");
        }
        try {
            //删除用户
            sysUser.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常,请稍后重试");
        }
    }

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     */
    public void updateUser(SysUser user) {
        if (null == user) {
            //用户信息为空
            throw new RuntimeException("请输入基本信息");
        }
        Integer id = user.getId();
        if (id == null) {
            //错误请求
            throw new RuntimeException("网络异常，请稍后重试");
        }
        //判断用户密码
        String password = user.getPassword();
        if (null == password || "".equals(password)) {
            //未输入密码
            password = sysUser.findById(id).get("password");
        }
        try {
            //根据id获取username
            String username = sysUser.findById(id).get("username");
            //将username设置到用户信息中
            user.setName(username);
            //将password设置到oldpassword中
            String oldPassword = sysUser.findById(id).get("password");
            user.setOldpassword(oldPassword);
            //更新
            user.update();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常，请稍后重试");
        }
    }

    /**
     * 查询用户信息
     * @param paginate 分页条件
     * @return 查询结果
     */
    public PageResult findUser(Paginate paginate) {
        try {
        //条件为空
        if (null==paginate){
            paginate = new Paginate();
        }
        //获取页码
        Integer currentPage = paginate.getCurrentPage();
        if (null == currentPage || currentPage <= 0) {
            //参数不符合标准设置默认值
            currentPage = 1;
        }
        //每页显示的条数
        Integer pageSize = paginate.getPageSize();
        if (null == pageSize || pageSize <= 0) {
            //参数不符合标准设置默认值
            pageSize = 10;
        }
        //查询条件
        String queryString1 = paginate.getQueryString();
        if (queryString1==null||"".equals(queryString1)){
            Page<SysUser> page = sysUser.paginate(currentPage, pageSize, "select *", "FROM sys_user" );
            return new PageResult(page.getTotalPage(),page.getTotalRow(),page.getList());
        }else {
            String queryString = "%" + paginate.getQueryString() + "%";
            Page<SysUser> page = sysUser.paginate(currentPage, pageSize, "select *", "FROM sys_user WHERE username LIKE ? OR name LIKE ?", queryString, queryString);
            return new PageResult(page.getTotalPage(), page.getTotalRow(), page.getList());
        }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常，请稍后重试");
        }

    }
}
