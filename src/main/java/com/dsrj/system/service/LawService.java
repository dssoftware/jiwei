package com.dsrj.system.service;

import com.dsrj.common.model.SysMenu;
import com.dsrj.common.model.YLaw;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/14 9:51
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class LawService {
    //根据id查
    public Record findLawBid(Integer id){
        return Db.findById("y_law", id);
    }
    //查分页
    public Page<Record> findLaw(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_law where 1=1 order by id desc");
    }
    public Page<Record> findLawAsContent(int pageNum, int rows,String name,String tiaokuan,String content){
        String s="from  y_law where";
        if(name!=null){
            s=s+" name= '"+name+"'";
            if(tiaokuan!=null){
                s=s+" and tiaokuan like '%"+tiaokuan+"%' ";
            }
            if(content!=null){
                s=s+"and content like '%"+content+"%'";
            }
        } else if(tiaokuan!=null){
            s=s+"tiaokuan like '%"+tiaokuan+"%'";
            if(content!=null) {
                s=s+"and content like '%"+content+"%'";
            }
        } else {
            if(content!=null) {
                s=s+" content like '%"+content+"%'";
            }
        }
        System.out.println(s);
        return Db.paginate(pageNum, rows, "select * ",
                s+" order by id desc");
    }
    //分类
    public List<Record> findLawName( ){
        return Db.find("select * from y_law where sort = 0");
    }
    //添加
    public boolean Add_Law(YLaw r){
        return r.save();
    }
    //修改
    public boolean Upd_Law(YLaw law){
        return law.update();
    }
    //删除
    public Boolean Del_LawBid(Integer id){
        return Db.deleteById("y_law",id);
    }

}
