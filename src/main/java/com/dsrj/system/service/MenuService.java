package com.dsrj.system.service;

import com.dsrj.common.model.SysMenu;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/8 17:06
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class MenuService {
    public boolean del_Menu(Integer id) {
        return Db.find("select * from sys_menu where father = "+id).size()>0?false:Db.deleteById("sys_menu",id);
    }
    public boolean ins_Menu(SysMenu menu) {
        return menu.save();
    }
    /*首先类型不允许修改，可以选中修改其父亲*/
    public boolean upd_Menu(SysMenu menu) {
        return menu.update();
    }
    //根据id查询具体的对象
    public Record sel_PerMenu(Integer param) {
        return Db.findById("sys_menu",param);
    }
    //查询所有菜单
    public List<Record> sel_AllMenu(){
        return Db.find("select * from sys_menu");
    }
    //查询所有父菜单
    public List<Record> sel_AllParMenu(){
        return Db.find("select * from sys_menu where father = 0 and enable = 1");
    }
    //查询父菜单下的可用的子菜单id
    public List<Record> sel_ChildMenu(Integer id){
        return Db.find("select * from sys_menu where enable = 1 and father = "+id);
    }

    //三表关联 根据roleid查询所有menu
    public List<Record> sel_MenuByRole(Integer id){
        return Db.find("select * from sys_menu where enable = 1 and id in (select menu_id from sys_role_menu where role_id ="+id+")");
    }
    //根据类型查询父菜单
    public List<Record> sel_MenuByType(Integer type){
        return Db.find("select * from sys_menu where enable = 1 and type = "+type);
    }
}
