package com.dsrj.system.service;

import com.dsrj.common.entity.PageResult;
import com.dsrj.common.entity.Paginate;
import com.dsrj.common.model.YXuser;
import com.jfinal.plugin.activerecord.Page;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * -----------------------------------
 * Describtion:嫌疑人操作业务层
 * -----------------------------------
 * Creatime:2020年01月14日
 * -----------------------------------
 * Author:mouzebin
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class XuserService {

    private YXuser xuser = new YXuser().dao();
    private final static String xls = "xls";
    private final static String xlsx = "xlsx";
    private final static String DATE_FORMAT = "yyyy/MM/dd";

    /**
     * 新增嫌疑人
     *
     * @param xuser
     */
    public void add(YXuser xuser) {
        if (xuser == null) {
            //用户信息为空
            throw new RuntimeException("请输入基本信息");
        }
        //处分
        String chufen = xuser.getChufen();
        if (chufen == null || "".equals(chufen)) {
            xuser.setChufen("无");
        }
        //奖惩情况
        String jiangcheng = xuser.getJiangcheng();
        if (jiangcheng == null || "".equals(jiangcheng)) {
            xuser.setJiangcheng("无");
        }
        //简历
        String jianli = xuser.getJianli();
        if (jianli == null || "".equals(jianli)) {
            xuser.setJianli("无");
        }
        //组织处理
        String zuzhichili = xuser.getZuzhichili();
        if (zuzhichili == null || "".equals(zuzhichili)) {
            xuser.setZuzhichili("无");
        }
        //人员基本情况
        String ryjbqk = xuser.getRyjbqk();
        if (ryjbqk == null || "".equals(ryjbqk)) {
            xuser.setRyjbqk("无");
        }
        //创建时间
        LocalDateTime dateTime = LocalDateTime.now();
        xuser.setCreateTime(dateTime.toString());
        xuser.save();
    }


    /**
     * 删除嫌疑人
     *
     * @param id
     */
    public void delUser(String id) {
        if (null == id || "".equals(id)) {
            //用户id为空
            throw new RuntimeException("网络异常,请稍后重试");
        }
        //判断是否为数字型
        boolean matches = Pattern.compile("^[-\\+]?[\\d]*$").matcher(id).matches();
        if (!matches){
            //id值不为数值型
            throw new RuntimeException("数据异常，请重新操作");
        }
        try {
            //删除用户
            xuser.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常,请稍后重试");
        }
    }

    /**
     * 修改嫌疑人信息
     *
     * @param xuser
     */
    public void updateUser(YXuser xuser) {
        if (xuser == null) {
            //用户信息为空
            throw new RuntimeException("请输入基本信息");
        }
        try {
            //案件编号不允许修改
            String bianhao = xuser.findById(xuser.getId()).getBianhao();
            xuser.setBianhao(bianhao);
            //更新时间戳
            LocalDateTime dateTime = LocalDateTime.now();
            xuser.setUpdateTime(dateTime.toString());
            xuser.update();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常，请稍后重试");
        }

    }


    /**
     * 查询嫌疑人
     *
     * @param paginate
     * @return
     */
    public PageResult findUser(Paginate paginate) {
        try {
            //参数为空
            if (null==paginate){
                paginate = new Paginate();
            }
            //获取页码
            Integer currentPage = paginate.getCurrentPage();
            if (null == currentPage || currentPage <= 0) {
                //参数不符合标准设置默认值
                currentPage = 1;
            }
            //每页显示的条数
            Integer pageSize = paginate.getPageSize();
            if (null == pageSize || pageSize <= 0) {
                //参数不符合标准设置默认值
                pageSize = 10;
            }
            //查询条件
            String queryString1 = paginate.getQueryString();
            if (queryString1==null||"".equals(queryString1)){
                Page<YXuser> page = xuser.paginate(currentPage, pageSize, "select *", "FROM y_xuser");
                return new PageResult(page.getTotalPage(), page.getTotalRow(), page.getList());
            }else {
                String queryString = "%" + paginate.getQueryString() + "%";
                Page<YXuser> page = xuser.paginate(currentPage, pageSize, "select *", "FROM y_xuser WHERE `name` LIKE ?", queryString);
                return new PageResult(page.getTotalPage(), page.getTotalRow(), page.getList());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常，请稍后重试");
        }
    }

    /**
     * 打印记录
     * @param id
     * @return
     */
    public YXuser pringLog(String id) {
        //验证数据是否为空
        if (null==id||"".equals(id)){
            throw new RuntimeException("网络异常，请稍后重试");
        }
        //判断是否为数字型
        boolean matches = Pattern.compile("^[-\\+]?[\\d]*$").matcher(id).matches();
        if (!matches){
            //id值不为数值型
            throw new RuntimeException("数据异常，请重新操作");
        }
        //查询数据库获取相关数据
        YXuser xuser = this.xuser.findById(id);
        return xuser;
    }


    /**
     * 将解析出的文件读取到数据库中
     *
     * @param file
     */
    public void upload(File file) {
        List<String[]> list = null;
        try {
            list = readExcel(file);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("网络异常，上传失败");
        }
        //循环获取数组中的数据
        for (String[] strings : list) {
            if (null == strings || strings.length <= 0) {
                //数组为空
                continue;
            }
            //姓名为空直接不允许保存
            if (null == strings[0] || "".equals(strings[0])) {
                continue;
            }
            YXuser yXuser = parsList(strings);
            yXuser.save();
        }
    }

    /**
     * 导出数据
     */
    public void download() {

        //模板所在路径
        String filePath = "src/main/webapp/download/renyuan/muban.xlsx";
        //查询数据库中的数据
        List<YXuser> xuserList = xuser.findAll();
        if (null == xuserList || xuserList.size() <= 0) {
            return;
        }
        //查询模板文件是否存在
        boolean exist = fileExist(filePath);
        if (exist) {
            //直接写入数据
            writeExcel(xuserList, filePath);
        } else {
            //先创建文件，再写入数据
            File file = new File(filePath);
            writeExcel(xuserList, filePath);
        }
    }


    /**
     * 读入excel文件，解析后返回
     *
     * @param file
     * @throws IOException
     */
    private List<String[]> readExcel(File file) throws IOException {
        //检查文件
        checkFile(file);
        //获得Workbook工作薄对象
        Workbook workbook = getWorkBook(file);
        //创建返回对象，把每行中的值作为一个数组，所有行作为一个集合返回
        List<String[]> list = new ArrayList<String[]>();
        if (workbook != null) {
            for (int sheetNum = 0; sheetNum < workbook.getNumberOfSheets(); sheetNum++) {
                //获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if (sheet == null) {
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                //循环除了第一行的所有行
                for (int rowNum = firstRowNum + 1; rowNum <= lastRowNum; rowNum++) {
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if (row == null) {
                        continue;
                    }
                    //获得当前行的开始列
                    int firstCellNum = row.getFirstCellNum();
                    //获得当前行的列数
                    int lastCellNum = row.getLastCellNum();
//                    int lastCellNum = row.getPhysicalNumberOfCells();
                    String[] cells = new String[24];
                    //循环当前行
                    for (int cellNum = firstCellNum; cellNum < lastCellNum; cellNum++) {
                        Cell cell = row.getCell(cellNum);
                        cells[cellNum] = getCellValue(cell);
                    }
                    list.add(cells);
                }
            }
            workbook.close();
        }
        return list;
    }

    //校验文件是否合法
    private void checkFile(File file) throws IOException {
        //判断文件是否存在
        if (null == file) {
            throw new FileNotFoundException("文件不存在！");
        }
        //获得文件名
        String fileName = file.getName();
        //判断文件是否是excel文件
        if (!fileName.endsWith(xls) && !fileName.endsWith(xlsx)) {
            throw new IOException(fileName + "不是excel文件");
        }
    }

    /**
     * 获取工作簿
     *
     * @param file
     * @return
     */
    private Workbook getWorkBook(File file) {
        //获得文件名
        String fileName = file.getName();
        //创建Workbook工作薄对象，表示整个excel
        Workbook workbook = null;
        try {
            //获取excel文件的io流
            InputStream is = new FileInputStream(file);
            //根据文件后缀名不同(xls和xlsx)获得不同的Workbook实现类对象
            if (fileName.endsWith(xls)) {
                //2003
                workbook = new HSSFWorkbook(is);
            } else if (fileName.endsWith(xlsx)) {
                //2007
                workbook = new XSSFWorkbook(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workbook;
    }

    /**
     * 获取单元格中的值
     *
     * @param cell
     * @return
     */
    private String getCellValue(Cell cell) {
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        //如果当前单元格内容为日期类型，需要特殊处理
        String dataFormatString = cell.getCellStyle().getDataFormatString();
        if (dataFormatString.equals("m/d/yy")) {
            cellValue = new SimpleDateFormat(DATE_FORMAT).format(cell.getDateCellValue());
            return cellValue;//"2019/10/10"
        }
        //把数字当成String来读，避免出现1读成1.0的情况
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        //判断数据的类型
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC: //数字
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING: //字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN: //Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA: //公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case Cell.CELL_TYPE_BLANK: //空值
                cellValue = "";
                break;
            case Cell.CELL_TYPE_ERROR: //故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

    //判断文件是否存在
    private boolean fileExist(String filePath) {
        boolean flag = false;
        File file = new File(filePath);
        flag = file.exists();
        return flag;
    }

    //向Excel中写数据
    private void writeExcel(List<YXuser> list, String filePath) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("xuser");
        XSSFRow firstRow = sheet.createRow(0);//第一行表头
        XSSFCell cells[] = new XSSFCell[24];

        String[] titles = new String[]{"姓名", "身份证号", "性别(男1女2)", "出生年月", "文化程度(初中以下1,高中2,大专3,本科4,硕士研究生5,博士研究生6)", "民族", "籍贯", "政治面貌", "入党时间", "参加工作时间", "单位", "身份(公务员1,参公人员2,事业编制人员3,国企职员4,农村党员5,临时工6,其他人员7)", "部门分类(党务1,政府2,监察3,人大4,政协5,审判6,检查7,人民团体8,事业单位9,企业单位10,农村11,其他12)", "党内职务", "任职时间", "行政职务", "任职时间", "其他职务", "任职时间", "在职情况(在职1,离职2,退休3)", "联系方式", "户籍地住址", "现住址", "工作简历"
        };
        //循环设置表头信息
        for (int i = 0; i < titles.length; i++) {
            cells[0] = firstRow.createCell(i);
            cells[0].setCellValue(titles[i]);
        }

        //遍历list,将数据写入Excel中
        for (int i = 0; i < list.size(); i++) {
            XSSFRow row = sheet.createRow(i + 1);
            YXuser yXuser = list.get(i);
            XSSFCell cell = row.createCell(0); //第1列
            cell.setCellValue(yXuser.getName());
            cell=row.createCell(1); //第2列
            cell.setCellValue(yXuser.getIdCard());
            cell=row.createCell(2); //第3列
            cell.setCellValue(yXuser.getSex());
            cell=row.createCell(3); //第4列
            cell.setCellValue(yXuser.getDate());
            cell=row.createCell(4); //第5列
            cell.setCellValue(yXuser.getWenhua());
            cell=row.createCell(5); //第6列
            cell.setCellValue(yXuser.getMinzu());
            cell=row.createCell(6); //第7列
            cell.setCellValue(yXuser.getGuanji());
            cell=row.createCell(7); //第8列
            cell.setCellValue(yXuser.getMianmao());
            cell=row.createCell(8); //第9列
            cell.setCellValue(yXuser.getRudangshijian());
            cell=row.createCell(9); //第10列
            cell.setCellValue(yXuser.getWork());
            cell=row.createCell(10); //第11列
            cell.setCellValue(yXuser.getCompany());
            cell=row.createCell(11); //第12列
            cell.setCellValue(yXuser.getShenfen());
            cell=row.createCell(12); //第13列
            cell.setCellValue(yXuser.getBumen());
            cell=row.createCell(13); //第14列
            cell.setCellValue(yXuser.getDnzw());
            cell=row.createCell(14); //第15列
            cell.setCellValue(yXuser.getDnsj());
            cell=row.createCell(15); //第16列
            cell.setCellValue(yXuser.getXzzw());
            cell=row.createCell(16); //第17列
            cell.setCellValue(yXuser.getXzsj());
            cell=row.createCell(17); //第18列
            cell.setCellValue(yXuser.getXzzw());
            cell=row.createCell(18); //第19列
            cell.setCellValue(yXuser.getQtzw());
            cell=row.createCell(19); //第20列
            cell.setCellValue(yXuser.getZzqk());
            cell=row.createCell(20); //第21列
            cell.setCellValue(yXuser.getContact());
            cell=row.createCell(21); //第22列
            cell.setCellValue(yXuser.getHjdzz());
            cell=row.createCell(22); //第23列
            cell.setCellValue(yXuser.getXzz());
            cell=row.createCell(23); //第24列
            cell.setCellValue(yXuser.getJianli());
        }
        OutputStream out = null;
        try {
            out = new FileOutputStream(filePath);
            workbook.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 接收字符串转换成对象
     *
     * @param str
     * @return
     */
    public static YXuser parsList(String[] str) {
        YXuser yXuser = new YXuser();
        //获取str里面的指定数据
        String name = str[0];
        String idCard = str[1];
        String sex = str[2];
        String date = str[3];
        String wenhua = str[4];
        String minzu = str[5];
        String guanji = str[6];
        String mianmao = str[7];
        String rudangshijian = str[8];
        String work = str[9];
        String company = str[10];
        String shenfen = str[11];
        String bumen = str[12];
        String dnzw = str[13];
        String dnsj = str[14];
        String xzzw = str[15];
        String xzsj = str[16];
        String qtzw = str[17];
        String qtsj = str[18];
        String zzqk = str[19];
        String contact = str[20];
        String hjdzz = str[21];
        String xzz = str[22];
        String jianli = str[23];
        //给对象设置参数
        yXuser.setName(name);
        yXuser.setIdCard(idCard);
        yXuser.setSex(sex);
        yXuser.setDate(date);
        yXuser.setWenhua(wenhua);
        yXuser.setMinzu(minzu);
        yXuser.setGuanji(guanji);
        yXuser.setMianmao(mianmao);
        yXuser.setRudangshijian(rudangshijian);
        yXuser.setWork(work);
        yXuser.setCompany(company);
        yXuser.setShenfen(shenfen);
        yXuser.setBumen(bumen);
        yXuser.setDnzw(dnzw);
        yXuser.setDnsj(dnsj);
        yXuser.setXzzw(xzzw);
        yXuser.setXzsj(xzsj);
        yXuser.setQtzw(qtzw);
        yXuser.setQtsj(qtsj);
        yXuser.setZzqk(zzqk);
        yXuser.setContact(contact);
        yXuser.setHjdzz(hjdzz);
        yXuser.setXzz(xzz);
        yXuser.setJianli(jianli);
        //处分
        String chufen = yXuser.getChufen();
        if (chufen == null || "".equals(chufen)) {
            yXuser.setChufen("无");
        }
        //奖惩情况
        String jiangcheng = yXuser.getJiangcheng();
        if (jiangcheng == null || "".equals(jiangcheng)) {
            yXuser.setJiangcheng("无");
        }
        //组织处理
        String jianli1 = yXuser.getJianli();
        if (jianli1 == null || "".equals(jianli1)) {
            yXuser.setJianli("无");
        }
        //组织处理
        String zuzhichili = yXuser.getZuzhichili();
        if (zuzhichili == null || "".equals(zuzhichili)) {
            yXuser.setZuzhichili("无");
        }
        //人员基本情况
        String ryjbqk = yXuser.getRyjbqk();
        if (ryjbqk == null || "".equals(ryjbqk)) {
            yXuser.setRyjbqk("无");
        }
        return yXuser;
    }


}
