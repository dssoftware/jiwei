package com.dsrj.system.service;

import com.dsrj.common.model.SysJobManager;
import com.dsrj.util.QuartzManager;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import org.quartz.Job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author 邢方辉
 * @Date 2020/1/11 15:25
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class JobManagerService {

    public List<Record> Sel_AllManager(){
        return Db.find("SELECT * FROM sys_job_manager WHERE 1=1 AND is_enabled = 'true'");
    }
    public Page<Record> Sel_JobManager(int pageNum,int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  sys_job_manager where 1=1 order by id desc");
    }
    //新增任务信息
    public boolean Add_JobManager(Record r){
        return Db.save("sys_job_manager", "id", r);
    }
    //修改任务信息
    public boolean Upd_JobManager(Record r){
        return Db.update("sys_job_manager", "id", r);
    }
    //根据id查询任务信息
    public Record Sel_JobById(String id){
        return Db.findById("sys_job_manager", id);
    }
    public boolean Translate(Record r){
        return Db.tx(() -> {
            String clazz = r.getStr("clazz");
            String is_enabled = r.getStr("is_enabled");
            String change_enable = "false".equals(is_enabled) ? "true" : "false";
            r.set("is_enabled", change_enable);
            Db.update("sys_job_manager", "id", r);

            Class<? extends Job> jobClazz = null;
            try {
                jobClazz = Class.forName(clazz).asSubclass(Job.class);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            if ("false".equals(change_enable)) {
                try {
                    if(r.getStr("trigger_name")!=null&&r.getStr("trigger_group")!=null){
                        System.out.println("zz");
                        QuartzManager.removeJob(r.getStr("job_name"), r.getStr("job_group"),r.getStr("trigger_name"),r.getStr("trigger_group"));
                    } else {
                        System.out.println("gg");
                        QuartzManager.removeJob(r.getStr("job_name"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                pan_duan(r);
            }
            return true;
        });
    }
    //判断要执行哪个addjob方法
    public static void pan_duan(Record job){
        if ("true".equals(job.getStr("is_enabled"))) {
            String className = job.getStr("clazz");
            Class<? extends Job> jobClazz = null;
            try {
                jobClazz = Class.forName(className).asSubclass(Job.class);
            } catch (Exception e) {
            }
            String name = job.getStr("job_name");
            String group = job.getStr("job_group");
            String cron_time = job.getStr("cron_time");
            Map<String, Object> map=new HashMap<>();
            if(job.getStr("parameter")!=null){
                String[] s=job.getStr("parameter").split(";");
                for(String str:s){
                    map.put(str.substring(0,str.indexOf(":")),str.substring(str.indexOf(":")+1,str.length()));
                }
            }
            System.out.println(map.toString());
            if(job.getStr("job_group")!=null&&job.getStr("trigger_name")!=null&&job.getStr("trigger_group")!=null&&job.getStr("parameter")!=null){
                QuartzManager.addJob(name,job.getStr("job_group"),job.getStr("trigger_name"),job.getStr("trigger_group"),jobClazz, cron_time,map);
            } else if(job.getStr("job_group")!=null&&job.getStr("trigger_name")!=null&&job.getStr("trigger_group")!=null){
                QuartzManager.addJob(name,job.getStr("job_group"),job.getStr("trigger_name"),job.getStr("trigger_group"),jobClazz, cron_time);
            } else if(job.getStr("parameter")!=null){
                QuartzManager.addJob(name, jobClazz, cron_time,map);
            } else {
                QuartzManager.addJob(name, jobClazz, cron_time);
            }
        }
    }
}
