package com.dsrj.system.service;

import com.dsrj.common.model.SysMenu;
import com.dsrj.common.model.SysRole;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 15:45
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class RoleService {
    //增加角色
    public boolean addRole(SysRole role) {
        return role.save();
    }
    //修改角色
    public boolean updateRole(SysRole role) {
        return role.update();
    }
    //删除角色
    public boolean delRole(Integer id) {
        return Db.deleteById("sys_role",id);
    }
    //查询角色
    public List<Record> findAllRole(){
        return Db.find("select * from sys_role ");
    }
    //id查询角色
    public Record findRoleById(Integer id) {
        return Db.findById("sys_role",id);
    }
    //分页查询
    public Page<Record> findRole(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  sys_role where 1=1 order by id desc");
    }
}
