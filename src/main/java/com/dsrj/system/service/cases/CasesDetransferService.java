package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesDetransfer;
import com.dsrj.common.model.YCasesRycl;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:46
 * @Version 1.0
 * @Content 创建一个java文本  移送部门
 */
public class CasesDetransferService {
    //增加
    public boolean ins_YCasesDetransfer(YCasesDetransfer cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesDetransfer(YCasesDetransfer cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesDetransfer(Integer id) {
        return Db.deleteById("y_cases_detransfer",id);
    }
    //查询
    public List<Record> sel_AllYCasesDetransfer(){
        return Db.find("select * from y_cases_detransfer ");
    }
    //分页查询
    public Page<Record> sel_YCasesDetransfer(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_detransfer where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesDetransferBycid(Integer cid){
        return Db.find("select * from y_cases_detransfer where cid = "+cid);
    }
}
