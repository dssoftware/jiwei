package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesTanhua;
import com.dsrj.common.model.YCasesTanhuaData;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:51
 * @Version 1.0
 * @Content 创建一个java文本  谈话数据
 */
public class CasesTanhuaDataService {
    //增加
    public boolean ins_YCasesTanhuaData(YCasesTanhuaData cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesTanhuaData(YCasesTanhuaData cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesTanhuaData(Integer id) {
        return Db.deleteById("y_cases_tanhua_data",id);
    }
    //查询
    public List<Record> sel_AllYCasesTanhuaData(){
        return Db.find("select * from y_cases_tanhua_data ");
    }
    //分页查询
    public Page<Record> sel_YCasesTanhuaData(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_tanhua_data where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesTanhuaDataBycid(Integer cid){
        return Db.find("select * from y_cases_tanhua_data where cid = "+cid);
    }
}
