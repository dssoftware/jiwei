package com.dsrj.system.service.cases;

import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.YCasesLevel;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 17:17
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesLevelService {
    //增加角色
    public boolean ins_CasesLevel(YCasesLevel level) {
        return level.save();
    }
    //修改角色
    public boolean upd_CasesLevel(YCasesLevel level) {
        return level.update();
    }
    //删除角色
    public boolean del_CasesLevel(Integer id) {
        return Db.deleteById("y_cases_level",id);
    }
    //查询角色
    public List<Record> sel_AllCasesLevel(){
        return Db.find("select * from y_cases_level ");
    }
    //分页查询
    public Page<Record> sel_CasesLevel(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_level where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_CasesLevelBycid(Integer cid){
        return Db.find("select * from y_cases_level where cid = "+cid);
    }
}
