package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesTanhuaData;
import com.dsrj.common.model.YCasesYisong;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:51
 * @Version 1.0
 * @Content 创建一个java文本 移送其他部门
 */
public class CasesYisongService {
    //增加
    public boolean ins_YCasesYisong(YCasesYisong cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesYisong(YCasesYisong cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesYisong(Integer id) {
        return Db.deleteById("y_cases_yisong",id);
    }
    //查询
    public List<Record> sel_AllYCasesYisong(){
        return Db.find("select * from y_cases_yisong ");
    }
    //分页查询
    public Page<Record> sel_YCasesYisong(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_yisong where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesYisongBycid(Integer cid){
        return Db.find("select * from y_cases_yisong where cid = "+cid);
    }
}
