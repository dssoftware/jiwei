package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesYuyi;
import com.dsrj.common.model.YCasesZancun;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:52
 * @Version 1.0
 * @Content 创建一个java文本  暂存
 */
public class CasesZancunService {
    //增加
    public boolean ins_YCasesZancun(YCasesZancun cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesZancun(YCasesZancun cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesZancun(Integer id) {
        return Db.deleteById("y_cases_zancun",id);
    }
    //查询
    public List<Record> sel_AllYCasesZancun(){
        return Db.find("select * from y_cases_zancun ");
    }
    //分页查询
    public Page<Record> sel_YCasesZancun(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_zancun where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesZancunBycid(Integer cid){
        return Db.find("select * from y_cases_zancun where cid = "+cid);
    }
}
