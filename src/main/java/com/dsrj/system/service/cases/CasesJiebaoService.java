package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesDeturn;
import com.dsrj.common.model.YCasesJiebao;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:48
 * @Version 1.0
 * @Content 创建一个java文本  案件结报
 */
public class CasesJiebaoService {
    //增加
    public boolean ins_YCasesJiebao(YCasesJiebao cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesJiebao(YCasesJiebao cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesJiebao(Integer id) {
        return Db.deleteById("y_cases_jiebao",id);
    }
    //查询
    public List<Record> sel_AllYCasesJiebao(){
        return Db.find("select * from y_cases_jiebao ");
    }
    //分页查询
    public Page<Record> sel_YCasesJiebao(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_jiebao where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesJiebaoBycid(Integer cid){
        return Db.find("select * from y_cases_jiebao where cid = "+cid);
    }
}
