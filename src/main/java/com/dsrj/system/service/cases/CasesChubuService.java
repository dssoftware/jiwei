package com.dsrj.system.service.cases;

import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.YCasesChubu;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 17:13
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesChubuService {
    //增加角色
    public boolean ins_CasesChubu(YCasesChubu cases) {
        return cases.save();
    }
    //修改角色
    public boolean upd_CasesChubu(YCasesChubu cases) {
        return cases.update();
    }
    //删除角色
    public boolean del_CasesChubu(Integer id) {
        return Db.deleteById("y_cases_chubu",id);
    }
    //查询角色
    public List<Record> sel_AllCasesChubu(){
        return Db.find("select * from y_cases_chubu ");
    }
    //分页查询
    public Page<Record> sel_CasesChubu(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_chubu where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_CasesChubuBycid(Integer cid){
        return Db.find("select * from y_cases_chubu where cid = "+cid);
    }
}
