package com.dsrj.system.service.cases;

import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.YCases;
import com.dsrj.common.model.YCasesRy;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 17:12
 * @Version 1.0
 * @Content 创建一个java文本 线索涉及人员管理
 */
public class CasesRyService {
    //增加角色
    public boolean ins_CasesRy(YCasesRy cases) {
        return cases.save();
    }
    //修改角色
    public boolean upd_CasesRy(YCasesRy cases) {
        return cases.update();
    }
    //删除角色
    public boolean del_CasesRy(Integer id) {
        return Db.deleteById("y_cases_ry",id);
    }
    //查询角色
    public List<Record> sel_AllCasesRy(){
        return Db.find("select * from y_cases_ry ");
    }
    //分页查询
    public Page<Record> sel_CasesRy(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_ry where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_CasesRyBycid(Integer cid){
        return Db.find("select * from y_cases_ry where cid = "+cid);
    }
}
