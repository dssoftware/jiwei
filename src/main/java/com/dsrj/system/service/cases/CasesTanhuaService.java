package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesRycl;
import com.dsrj.common.model.YCasesTanhua;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:50
 * @Version 1.0
 * @Content 创建一个java文本  线索谈话
 */
public class CasesTanhuaService {
    //增加
    public boolean ins_YCasesTanhua(YCasesTanhua cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesTanhua(YCasesTanhua cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesTanhua(Integer id) {
        return Db.deleteById("y_cases_tanhua",id);
    }
    //查询
    public List<Record> sel_AllYCasesTanhua(){
        return Db.find("select * from y_cases_tanhua ");
    }
    //分页查询
    public Page<Record> sel_YCasesTanhua(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_tanhua where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesTanhuaBycid(Integer cid){
        return Db.find("select * from y_cases_tanhua where cid = "+cid);
    }
}
