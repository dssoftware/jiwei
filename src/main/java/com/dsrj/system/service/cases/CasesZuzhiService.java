package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesZancun;
import com.dsrj.common.model.YCasesZuzhi;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:52
 * @Version 1.0
 * @Content 创建一个java文本 组织处理
 */
public class CasesZuzhiService {
    //增加
    public boolean ins_YCasesZuzhi(YCasesZuzhi cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesZuzhi(YCasesZuzhi cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesZuzhi(Integer id) {
        return Db.deleteById("y_cases_zuzhi",id);
    }
    //查询
    public List<Record> sel_AllYCasesZuzhi(){
        return Db.find("select * from y_cases_zuzhi ");
    }
    //分页查询
    public Page<Record> sel_YCasesZuzhi(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_zuzhi where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesZuzhiBycid(Integer cid){
        return Db.find("select * from y_cases_zuzhi where cid = "+cid);
    }
}
