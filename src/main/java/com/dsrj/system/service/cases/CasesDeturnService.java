package com.dsrj.system.service.cases;

import com.dsrj.common.model.YCasesDeturn;
import com.dsrj.common.model.YCasesRy;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/16 10:47
 * @Version 1.0
 * @Content 创建一个java文本 线索-转部门办理表
 */
public class CasesDeturnService {
    //增加
    public boolean ins_YCasesDeturn(YCasesDeturn cases) {
        return cases.save();
    }
    //修改
    public boolean upd_YCasesDeturn(YCasesDeturn cases) {
        return cases.update();
    }
    //删除
    public boolean del_YCasesDeturn(Integer id) {
        return Db.deleteById("y_cases_deturn",id);
    }
    //查询
    public List<Record> sel_AllYCasesDeturn(){
        return Db.find("select * from y_cases_deturn ");
    }
    //分页查询
    public Page<Record> sel_YCasesDeturn(int pageNum, int rows){
        return Db.paginate(pageNum, rows, "select * ",
                " from  y_cases_deturn where 1=1 order by id desc");
    }
    //根据问题线索cid查询
    public List<Record> sel_YCasesDeturnBycid(Integer cid){
        return Db.find("select * from y_cases_deturn where cid = "+cid);
    }
}
