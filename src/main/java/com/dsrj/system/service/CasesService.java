package com.dsrj.system.service;

import com.dsrj.common.model.YCases;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * @Author 邢方辉
 * @Date 2020/1/14 14:06
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class CasesService {
    //增
    public  boolean ins_YCases(YCases yCases){
        return yCases.save();
    }
    //删
    public boolean del_YCase(Integer id) {
        return Db.deleteById("y_cases",id);
    }
    //改
    public boolean upd_YCase(YCases yCases){
        return yCases.update();
    }
    //查分页+条件分页
    public Page<Record> sel_caseAsContent(int pageNum, int rows, String xsbh, String xsmc, String sldjrq1, String sldjrq2){
        String s="from  y_cases where";
        if(xsbh!=null){
            s=s+"xsbh="+xsbh;
            if(xsmc!=null){
                s=s+" and xsmc like '%"+xsmc+"%' ";
            }
            if(sldjrq1!=null){
                s=s+"and sldjrq between ("+sldjrq1+" and "+sldjrq2+")";
            }
        } else if(xsmc!=null){
            s=s+"xsmc like '%"+xsmc+"%'";
            if(sldjrq1!=null){
                s=s+"and sldjrq between "+sldjrq1+" and "+sldjrq2+")";
            }
        } else {
            if(sldjrq1!=null){
                s=s+" sldjrq between ("+sldjrq1+" and "+sldjrq2+")";
            }
        }
        return Db.paginate(pageNum, rows, "select * ",
                s+" order by id desc");
    }
    //多表查询部分  与y_cases_ry  y_user
    //查询与线索有关的涉案人员
    //查询具体某用户的涉及案件列表  分页
}
