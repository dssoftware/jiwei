package com.dsrj.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.SysRoleMenu;
import com.dsrj.system.service.RoleMenuService;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;

import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 15:57
 * @Version 1.0
 * @Content 创建一个java文本  角色菜单管理
 */
public class RoleMenuController extends BaseController {
    @Inject
    private RoleMenuService service;
    //增加创建关联
    public void insRoleMenu() {
        String jsonStr = HttpKit.readData(getRequest());
        SysRoleMenu role = JSONObject.parseObject(jsonStr, SysRoleMenu.class);
        if(service.ins_RoleMenu(role)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //修改关联
    public void updRoleMenu( ) {
        String jsonStr = HttpKit.readData(getRequest());
        SysRoleMenu role = JSONObject.parseObject(jsonStr, SysRoleMenu.class);
        if(service.upd_RoleMenu(role)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //删除关联
    public void delRoleMenu() {
        if(service.del_RoleMenu(getParaToInt(0))){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //查询关联
    public void selAllRoleMenu(){
        Rj(ConstCode.SUCCESS,service.sel_AllRoleMenu());
    }
    //分页查询
    //分页查询s
    public void selRole(){
        int pageNum = getParaToInt("page", 1);
        int rows = getParaToInt("rows", 10);
        Rj(ConstCode.SUCCESS,service.sel_RoleMenu(pageNum,rows));
    }
}
