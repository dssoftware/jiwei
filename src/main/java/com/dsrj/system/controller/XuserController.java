package com.dsrj.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.PageResult;
import com.dsrj.common.entity.Paginate;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YXuser;
import com.dsrj.system.service.XuserService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.HttpKit;
import com.jfinal.upload.UploadFile;

import java.io.File;

/**
 * -----------------------------------
 * Describtion:嫌疑人操作
 * -----------------------------------
 * Creatime:2020年01月14日
 * -----------------------------------
 * Author:mouzebin
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class XuserController extends BaseController {

    @Inject
    private XuserService xuserService;

    //文件名
    private static final String fName = "renyuan";

    //文件上传路径
    private static final String filePath = "/renyuan/upload";

    /**
     * 增加用户
     */
    public void add() {
        //获取输入的用户信息
        String jsonStr = HttpKit.readData(getRequest());
        YXuser user = JSONObject.parseObject(jsonStr, YXuser.class);
        try {
            //调用方法存储用户信息
            xuserService.add(user);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            //操作失败
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 删除用户
     */
    public void delUser() {
        //获取用户id
        String id = getPara("id");
        //调用删除方法执行删除
        try {
            //执行成功
            xuserService.delUser(id);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 修改用户
     */
    public void updateUser() {
        //获取输入的用户信息
        String jsonStr = HttpKit.readData(getRequest());
        YXuser xuser = JSONObject.parseObject(jsonStr, YXuser.class);
        //调用修改方法
        try {
            //修改成功
            xuserService.updateUser(xuser);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 查询用户
     */
    public void findUser() {
        //获取分页信息
        String jsonStr = HttpKit.readData(getRequest());
        Paginate paginate = JSONObject.parseObject(jsonStr, Paginate.class);
        try {
            //查询用户信息返回数据
            PageResult result = xuserService.findUser(paginate);
           Rj(ConstCode.DO_SUCCESS, result);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 打印记录
     */
    public void printLog(){
        String id = getPara("id");
        //根据id获取需要打印的数据
        try {
            YXuser result = xuserService.pringLog(id);
            Rj(ConstCode.DO_SUCCESS, result);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 文件上传
     */
    public void uploadXuser() {

        //文件上传
        UploadFile file = getFile("file", filePath, 10 * 1024 * 1024, "utf-8");
        //解析文件中的数据
        try {
            xuserService.upload(file.getFile());
           Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        } finally {
            //删除文件
            file.getFile().delete();
        }
    }

    /**
     * 导出文件
     */
    public void downloadXuser() {
        try {
            //调用导出方法
            xuserService.download();
            renderFile("renyuan/muban.xlsx");
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }finally {
            //模板所在路径
            String filePath = "src/main/webapp/download/renyuan/muban.xlsx";
            boolean exist = fileExist(filePath);
            if (exist){
                new File(filePath).exists();
            }
        }
    }

    //判断文件是否存在
    private boolean fileExist(String filePath) {
        boolean flag = false;
        File file = new File(filePath);
        flag = file.exists();
        return flag;
    }
}
