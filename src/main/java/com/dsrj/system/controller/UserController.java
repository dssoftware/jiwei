package com.dsrj.system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.PageResult;
import com.dsrj.common.entity.Paginate;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.SysMenu;
import com.dsrj.common.model.SysUser;
import com.dsrj.system.annotation.Log;
import com.dsrj.system.service.UserService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

/**
 * -----------------------------------
 * Describtion:系统用户管理
 * -----------------------------------
 * Creatime:2020年1月9日
 * -----------------------------------
 * Author:Mile Chan
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class UserController extends BaseController {

    @Inject
    private UserService userService;

    /**
     * 增加用户
     */
    public void add() {
        //获取输入的用户信息
        String jsonStr = HttpKit.readData(getRequest());
        SysUser user = JSONObject.parseObject(jsonStr, SysUser.class);
        try {
            //调用方法存储用户信息
            userService.add(user);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            //操作失败
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 删除用户
     */
    public void delUser() {
        //获取用户id
        String id = getPara("id");
        //调用删除方法执行删除
        try {
            //执行成功
            userService.delUser(id);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 修改用户
     */
    public void updateUser() {
        //获取输入的用户信息
        String jsonStr = HttpKit.readData(getRequest());
        SysUser user = JSONObject.parseObject(jsonStr, SysUser.class);
        //调用修改方法
        try {
            //修改成功
            userService.updateUser(user);
            Rj(ConstCode.DO_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }

    /**
     * 查询用户
     */
    @Log("查询所有用户")
    public void findUser() {
        //获取分页信息
        String jsonStr = HttpKit.readData(getRequest());
        Paginate paginate = JSONObject.parseObject(jsonStr, Paginate.class);
        try {
            //查询用户信息返回数据
            PageResult result = userService.findUser(paginate);
            Rj(ConstCode.DO_SUCCESS,result);
        } catch (Exception e) {
            e.printStackTrace();
            Rj(ConstCode.DO_ERROR, e.getMessage());
        }
    }


}
