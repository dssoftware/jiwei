package com.dsrj.system.controller;

import com.alibaba.druid.util.StringUtils;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.system.service.JobManagerService;
import com.dsrj.util.QuartzManager;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;


/**
 * @author :  Xiaoy
 * @Description :  定时任务控制器
 * @CreateTime: 2019-06-25 15:53
 */
public class JobmanageController extends BaseController {
    @Inject
    private JobManagerService service;
    //整体启用
    public void init() throws ClassNotFoundException {
        QuartzManager qm=new QuartzManager();
        qm.initJob(service.Sel_AllManager());
        renderJson(new RD(ConstCode.SUCCESS,service.Sel_AllManager()));
    }
    //整体关停
    public void shut_down() {
        QuartzManager.shutdownJobs();
        renderJson(new RD(ConstCode.SUCCESS,null));
    }

    //启用关停
    public void start_end() {
        boolean bl = false;
        final String id = getPara("id");
        final Record r = service.Sel_JobById(id);
        try {
            if (r != null) {
                //事务管理，确保数据库状态与任务管理器一致
                bl=service.Translate(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(bl){
            renderJson(new RD(ConstCode.SUCCESS,r));
        } else{
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //修改任务执行频率
    public void modifyJobTime(){
        String job_name=getPara("job_name");
        String cron_time=getPara("cron_time");
        String trigger_name=getPara("trigger_name");
        String trigger_group=getPara("trigger_group");
        if(job_name!=null){
            QuartzManager.modifyJobTime(job_name,cron_time);
        } else {
            QuartzManager.modifyJobTime(trigger_name,trigger_group,cron_time);
        }

    }
    public void get() {
        Record r = null;
        try {
            String id = getPara("id");
            r = service.Sel_JobById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        renderJson(new RD(ConstCode.SUCCESS,r));
    }
    //获取任务信息
    public void post() {
        int pageNum = getParaToInt("page", 1);
        int rows = getParaToInt("rows", 10);
        pageNum = pageNum > 0 ? pageNum : 1;
        renderJson(new RD(ConstCode.SUCCESS,service.Sel_JobManager(pageNum,rows)));
    }

    //新增任务
    public void add() {
        String id = getPara("id");
        String job_name = getPara("job_name");
        String job_group = getPara("job_group");
        String clazz = getPara("clazz");
        String desc = getPara("desc");
        String cron_time = getPara("cron_time");
        String trigger_name = getPara("trigger_name");
        String trigger_group = getPara("trigger_group");
        String parameter = getPara("parameter");
        boolean result = false;
        try {
            Record r = new Record();
            r.set("job_name", job_name);
            r.set("job_group", job_group);
            r.set("clazz", clazz);
            r.set("desc",desc);
            r.set("cron_time", cron_time);
            r.set("is_enabled", "false");
            r.set("trigger_name",trigger_name);
            r.set("trigger_group", trigger_group);
            r.set("parameter", "parameter");
            if (StringUtils.isEmpty(id)) {//新增
                result=service.Add_JobManager(r);
            } else {
                //修改
                if(r.getStr("trigger_name")!=null&&r.getStr("trigger_group")!=null){
                    QuartzManager.removeJob(r.getStr("job_name"), r.getStr("job_group"),r.getStr("trigger_name"),r.getStr("trigger_group"));
                } else {
                    QuartzManager.removeJob(r.getStr("job_name"));
                }
                r.set("id", id);
                result=service.Upd_JobManager(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(result){
            renderJson(new RD(ConstCode.SUCCESS));
        } else{
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
}
