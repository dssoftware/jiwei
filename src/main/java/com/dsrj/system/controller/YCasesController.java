package com.dsrj.system.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.YCases;
import com.dsrj.system.service.CasesService;
import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

/**
 * @Author 邢方辉
 * @Date 2020/1/14 14:05
 * @Version 1.0
 * @Content 创建一个java文本 问题线索管理
 */
public class YCasesController extends BaseController {
    //cases阶段
    @Inject
    private CasesService service;
    //增  包括两种  第一种线索种类：问题线索  第二种信访件 1-7
    public  void ins_YCases(){
        UploadFile uploadFile=this.getFile();
        YCases ycase=this.getModel(YCases.class,"ycase");
        if(service.ins_YCases(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }

    }
    //删  根节点删除  也就是受理处置删除
    public void del_YCase() {
        if(service.del_YCase(getParaToInt(0))){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //改
    /*一、内容更新
    二、下一步执行更新
    三、退一步更新*/
    public void upd_YCase(){
        UploadFile uploadFile=this.getFile();
        YCases ycase=this.getModel(YCases.class,"ycase");
        if(service.upd_YCase(ycase)){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }
    //查分页+条件分页  条件为空就是普通分页
    public void sel_caseAsContent(){
        int pageNum=getParaToInt("pageNum");
        int rows=getParaToInt("rows");
        String xsbh=getPara("xsbh");
        String xsmc=getPara("xsmc");
        String sldjrq1=getPara("sldjrq1");
        String sldjrq2=getPara("sldjrq2");
        renderJson(new RD(ConstCode.SUCCESS,service.sel_caseAsContent(pageNum,rows,xsbh,xsmc,sldjrq1,sldjrq2)));
    }

    //分类处置
    /*也就是根据不同的clyj执行不同的service方法*/
}
