package com.dsrj.system.controller;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.system.util.BeiFenUtil;
import com.dsrj.util.QuartzManager;

import java.io.File;
import java.sql.SQLOutput;

/**
 * @Author 邢方辉
 * @Date 2020/1/11 9:33
 * @Version 1.0
 * @Content 数据库备份与还原  数据库定时任务的手动触发工作层
 */
public class UtilController extends BaseController{
    //备份
    public void BeiFen(){
        BeiFenUtil.BeiFen();
        renderJson(new RD(ConstCode.SUCCESS));
    }

    //还原
    public void Recover(){
        //传递参数通过哪个版本还原需要传参
        File file=new File("");
        String[] arr=file.getAbsolutePath().split("\\\\");
        String filepath="";
        for(String s:arr){
            filepath=filepath+s+"\\";
        }
        if(BeiFenUtil.Recover(filepath+"sql\\")){
            renderJson(new RD(ConstCode.SUCCESS));
        } else {
            renderJson(new RD(ConstCode.FEAILED));
        }
    }

}
