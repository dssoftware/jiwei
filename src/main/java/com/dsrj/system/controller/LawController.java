package com.dsrj.system.controller;
import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.SysRoleMenu;
import com.dsrj.common.model.YLaw;
import com.dsrj.system.service.LawService;
import com.dsrj.system.util.LawUtil;
import com.jfinal.aop.Inject;
import com.jfinal.kit.HttpKit;
import com.jfinal.upload.UploadFile;
import java.io.File;
import java.util.List;

/**
 * @Author 邢方辉
 * @Date 2020/1/14 9:56
 * @Version 1.0
 * @Content 创建一个java文本 法律法规文件的管理
 */
public class LawController extends BaseController{
    @Inject
    private LawService service;
    public void selLawBid(){
        Rj(ConstCode.SUCCESS,service.findLawBid(getParaToInt("id")));
    }
    //查分页
    public void selLaw( ){
        int pageNum = getParaToInt("page", 1);
        int rows = getParaToInt("rows", 10);
        Rj(ConstCode.SUCCESS,service.findLaw(pageNum,rows));
    }
    //条件模糊查询
    //查分页
    public void selLawAsContent( ){
        int pageNum = getParaToInt("page", 1);
        int rows = getParaToInt("rows", 10);
        String name = getPara("name");
        String tiaokuan = getPara("tiaokuan");
        String content = getPara("content");
        System.out.println("pageNum:"+pageNum+",rows:"+rows+",name:"+name+",tiaokuan:"+tiaokuan+",content"+content);
        Rj(ConstCode.SUCCESS,service.findLawAsContent(pageNum,rows,name,tiaokuan,content));
    }
    //分类
    public void selectLawName( ){
        Rj(ConstCode.SUCCESS,service.findLawName());
    }
    //添加
    public void addLaw( ){
        UploadFile uploadFile=this.getFile();
        YLaw law=this.getModel(YLaw.class,"law");
        law.setCreateTime(System.currentTimeMillis()/1000);
        law.setUpdateTime(System.currentTimeMillis()/1000);
        if(service.Add_Law(law)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //修改
    public void updateLaw( ){
        String jsonStr = HttpKit.readData(getRequest());
        YLaw law = JSONObject.parseObject(jsonStr, YLaw.class);
        law.setUpdateTime(System.currentTimeMillis()/1000);
        if(service.Upd_Law(law)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //删除
    public void delLawBid( ){
        if(service.Del_LawBid(getParaToInt(0))) {
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //导入excel数据
    public void ImpLaw( ){
        UploadFile up=getFile("file","/law/upload",10*1024*1024,"utf-8");
        File file=up.getFile();
        try {
            List<String[]> list=LawUtil.readExcel(file);
            System.out.println(list.size());
            for(int status=0;status<list.size();status++){
                YLaw law= LawUtil.parstList(list.get(status));
                law.setSort(status);
                law.setCreateTime(System.currentTimeMillis()/1000);
                law.setUpdateTime(System.currentTimeMillis()/1000);
                service.Add_Law(law);
            }
            Rj(ConstCode.SUCCESS);
        } catch (Exception e) {
            Rj(ConstCode.FEAILED);
        }
    }
}
