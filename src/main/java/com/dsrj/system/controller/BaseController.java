package com.dsrj.system.controller;

import java.util.Map;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.SysUser;
import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jfinal.kit.Ret;

/**
-----------------------------------
*Describtion:controller 层父类  所有对外开发接口 映射都需要 集成该类
*-----------------------------------
*Creatime:2020年1月8日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class BaseController extends Controller {
	
	private SysUser user=null;
	
	private String token=null;
	
	private Ret R=null;
	
	/**
	 * 返回JSON数据到前端
	 * @param constcode
	 * @param data
	 */
	@NotAction
	public  void Rj(ConstCode constcode, Object data){
		renderJson(new Ret().set("data", data).set("code", constcode.getCode()).set("msg", constcode.getMsg()).set("status", constcode.getStatus()).set(get_R()));
	}
	
	@NotAction
	public  void Rj(ConstCode constcode){
		renderJson(new Ret().set("code", constcode.getCode()).set("msg", constcode.getMsg()).set("status", constcode.getStatus()).set(get_R()));
	}
	
	@NotAction
	public Ret get_R() {
		return R;
	}
	
	@NotAction
	public void R_attr(String key,Object obj){
		this.R.set(key, obj);
		
	}
	
	@NotAction
	public void R_ok(){
		this.R.set("state", "ok");
		
	}
	
	@NotAction
	public void R_fail(){
		this.R.set("state", "fail");
		
	}

	@NotAction
	public void set_R(Ret _R) {
		this.R = _R;
	}

	@NotAction
	public String getToken() {
		return token;
	}

	@NotAction
	public void setToken(String token) {
		this.token = token;
	}

	@NotAction
	public SysUser getUser() {
		return user;
	}

	@NotAction
	public void setUser(SysUser user) {
		this.user = user;
	}
	
	@NotAction
	public boolean isLogin() {
		return getUser() != null;
	}
	@NotAction
	public boolean notLogin() {
		return !isLogin();
	}
	
	/**
	 * 判断是否为AJAX 请求
	 * @return
	 */
	@NotAction
	public boolean isAjax() {
		String requestType = getRequest().getHeader("X-Requested-With");
		return "XMLHttpRequest".equals(requestType);
	}
	/**
	 * 普通操作  ，返回给前端。请求操作 状态,不带数据
	 * 
	 * @param is_success
	 */
	@NotAction
	public void RD(boolean is_success){
		if(is_success)
			renderJson(new RD(ConstCode.SUCCESS));
		else
			renderJson(new RD(ConstCode.FEAILED));
	}
	
	/**
	 * 普通操作  ，返回给前端。请求操作 状态,带数据
	 * 
	 * @param is_success
	 */
	@NotAction
	public void RD(boolean is_success,Object data){
		if(is_success)
			renderJson(new RD(ConstCode.SUCCESS,data));
		else
			renderJson(new RD(ConstCode.FEAILED,data));
	}
	
	/**
	 * 自定义 返回 状态 ,不带数据
	 * 
	 * @param is_success
	 */
	@NotAction
	public void RD(ConstCode constcode){
			renderJson(new RD(constcode));
	}

	/**
	 * 自定义 返回 状态 ,带数据
	 * 
	 * @param is_success
	 */
	@NotAction
	public void RD(ConstCode constcode,Object data){
			renderJson(new RD(constcode,data));
	}


	}
