package com.dsrj.system.controller;
import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.model.SysMenu;
import com.dsrj.system.service.MenuService;
import com.jfinal.aop.Inject;
import com.jfinal.kit.HttpKit;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author 邢方辉
 * @Date 2020/1/8 17:02
 * @Version 1.0
 * @Content 创建一个java文本 菜单管理模块
 */
public class MenuController extends BaseController{
    @Inject
    private MenuService service;
    /*界面初始化时查询所有父类菜单*/
    public void selectParent(){
        Rj(ConstCode.SUCCESS,service.sel_AllParMenu());
    }
    /*根据父菜单id查询子菜单id   url/参数*/
    public void selectChildById(){
        Rj(ConstCode.SUCCESS,service.sel_ChildMenu(getParaToInt(0)));
    }
    /*查询所有菜单*/
    public void findAllMenu(){
       Rj(ConstCode.SUCCESS,service.sel_AllMenu());
    }
    /*根据具体id进行删除操作
    ajaxget请求/格式
    */
    public void deleteById(){
        if(service.del_Menu(getParaToInt(0))){
            Rj(ConstCode.SUCCESS);
        }else{
            Rj(ConstCode.FEAILED);
        }
    }
    /*根据id进行具体查询操作*/
    public void selectMenuById(){
        Rj(ConstCode.FEAILED,service.sel_PerMenu(getParaToInt(0)));
    }
    /*根据form表单提交数据进行添加功能*/
    public void add() {
        String jsonStr = HttpKit.readData(getRequest());
        SysMenu menu = JSONObject.parseObject(jsonStr, SysMenu.class);
        long l = System.currentTimeMillis();
        Date date = new Date(l);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        menu.setCreatedate(dateFormat.format(date));
        menu.setEnable(0);
        if (service.ins_Menu(menu)) {
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    /*根据更新表单提交数据进行更改功能*/
    public void updateMenu() throws InstantiationException, IllegalAccessException {
        String jsonStr = HttpKit.readData(getRequest());
        SysMenu menu = JSONObject.parseObject(jsonStr, SysMenu.class);
        if(service.upd_Menu(menu)){
            Rj(ConstCode.SUCCESS);
        } else{
            Rj(ConstCode.FEAILED);
        }
    }
}
