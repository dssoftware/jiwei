package com.dsrj.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.common.model.SysMenu;
import com.dsrj.common.model.SysRole;
import com.dsrj.system.service.MenuService;
import com.dsrj.system.service.RoleMenuService;
import com.dsrj.system.service.RoleService;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.upload.UploadFile;

import javax.management.relation.Role;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author 邢方辉
 * @Date 2020/1/15 15:56
 * @Version 1.0
 * @Content 创建一个java文本  用户身份管理某块
 */
public class RoleController extends BaseController {
    @Inject
    private RoleService service;
    @Inject
    private RoleMenuService roleMenuService;
    @Inject
    private MenuService menuService;
    //增加角色
    public void insRole( ) {
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String jsonStr = HttpKit.readData(getRequest());
        SysRole role = JSONObject.parseObject(jsonStr, SysRole.class);
        role.setCreatedate(sdf.format(date));
        role.setEnable(0);
        if(service.addRole(role)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //修改角色
    public void updateRole() {
        String jsonStr = HttpKit.readData(getRequest());
        SysRole role = JSONObject.parseObject(jsonStr, SysRole.class);
        if(service.updateRole(role)){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //删除角色 url/id
    public void deleteRole( ) {
        if(roleMenuService.sel_RoleMenuBid(getParaToInt(0)).size()==0&&service.delRole(getParaToInt(0))){
            Rj(ConstCode.SUCCESS);
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
    //查询角色
    public void selAllRole(){
        Rj(ConstCode.FEAILED,service.findAllRole());
    }
    //分页查询
    public void selRoleFenYe(){
        int pageNum = getParaToInt("page", 1);
        int rows = getParaToInt("rows", 10);
        Rj(ConstCode.FEAILED,service.findRole(pageNum,rows));
    }
    //根据roleid 查询可以操作的菜单
    public void selRoleById(){
        if(service.findRoleById(getParaToInt(0)).getInt("enable")==1){
            Rj(ConstCode.SUCCESS,menuService.sel_MenuByRole(getParaToInt(0)));
        } else {
            Rj(ConstCode.FEAILED);
        }
    }
}
