package com.dsrj.system.interceptor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.dsrj.common.model.SysMenu;
import com.dsrj.common.model.SysRole;
import com.dsrj.common.model.SysRoleMenu;
import com.dsrj.common.model.SysUser;
import com.dsrj.common.model.SysUserRole;
import com.dsrj.system.annotation.RequiresPerms;
import com.dsrj.system.controller.BaseController;
import com.dsrj.util.JwtUtil;
import com.jfinal.aop.Inject;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;

import io.jsonwebtoken.Claims;

/**
-----------------------------------
*Describtion:后端访问 身份认证拦截
*-----------------------------------
*Creatime:2020年1月16日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class AuthInterceptor implements Interceptor {

	@Inject
	private SysRole sysRole;
	
	@Inject
	private SysMenu sysMenu;
	
	@Inject
	private SysRoleMenu sysRoleMenu;
	
	@Inject
	private SysUserRole  sysUserRole;
	
	@Override
	public void intercept(Invocation inv) {
		BaseController c=(BaseController)inv.getController();
		
		//获取注解
        Method method = inv.getMethod();
        RequiresPerms perms = method.getAnnotation(RequiresPerms.class);
		//通过 user_id 获取角色=用户
		List<SysUserRole> listSysUserRole=sysUserRole.find("select * from sys_user_role where user_id=?",c.getUser().getId());
		if(listSysUserRole.size()<1){
			c.renderJson(Ret.ok("msg","此用户无角色"));
			return ;
		}
		//获取权限
		List<SysRoleMenu> listRoleMenu=new ArrayList<>();
		
		for(SysUserRole x:listSysUserRole){
			listRoleMenu.addAll(sysRoleMenu.find("select * from sys_role_menu where role_id=?",x.getRoleId()));
		}
		if(listRoleMenu.size()<1){
			c.renderJson(Ret.ok("msg","此用户有角色无权限"));
			return ;
		}
		//获取权限
		List<SysMenu> listSysMenu=new ArrayList<>();
		for(SysRoleMenu x:listRoleMenu){
			listSysMenu.add(sysMenu.findById(x.getMenuId()));
		}
		
		//如果注解不为空 则对具体操作权限级别做验证
		if(perms!=null){
		if(!isAllow(listSysMenu,perms.value())){
			c.renderJson(Ret.fail("msg","暂无权访问，请联系管理员！"));
			return ;
		}}
				
		inv.invoke();

	}

	private boolean isAllow(List<SysMenu> listSysMenu, String value) {
		boolean flag=false;
		for(SysMenu x:listSysMenu){
			if(value.equals(x.getPerms())){
				 flag=true;
				 break;
			}
		}
		return flag;
		
	}

}
