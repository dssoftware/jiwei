package com.dsrj.system.interceptor;

import java.util.Map;

import com.dsrj.common.model.SysUser;
import com.dsrj.system.controller.BaseController;
import com.dsrj.util.JwtUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;

import io.jsonwebtoken.Claims;

/**
-----------------------------------
*Describtion:全局拦截器  只能对action请求起作用  可以访问Action的上下文，值栈里的对象
*-----------------------------------
*Creatime:2020年1月8日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:登录拦截-》获取token=>获取用户
*-----------------------------------
**/
public class GlobolInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		//获取Token信息 存入全局、解密得到用户信息存入全局、通过用户信息获取用户权限存入全局
		BaseController c=(BaseController)inv.getController();
		SysUser user=new SysUser();
		Ret r=new Ret();
		String token=c.get("token");
		if(token==null || token==""){
			inv.invoke();
			c.renderJson(Ret.fail("msg", "token不能为空"));
			return;
		}
		//校验token
		if(!JwtUtil.isVerify(token)){
			c.renderJson(Ret.fail("msg", "token失效请重新登录"));
			return ;
		}
		//获取登录用户信息
		Claims  clams=JwtUtil.parseJWT(token);
		int id=Integer.parseInt(clams.get("id").toString());
		user.setId(id);
		user.setUsername(clams.get("username").toString());
		user.setPassword(clams.get("password").toString());
		c.setUser(user);
		//用户JWT验证成功后 重置有效时间
		long liveMillis=60*60*1000;
		Map<String,Object> jwt=JwtUtil.createJWTwithTime(liveMillis, user.getId().toString(), user.getUsername(), user.getPassword());
		r.set("token", jwt.get("token"));
		r.set("deadline", jwt.get("deadline"));
		c.set_R(r);
		inv.invoke();
	}

}
