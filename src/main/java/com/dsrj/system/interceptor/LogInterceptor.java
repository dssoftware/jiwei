package com.dsrj.system.interceptor;

import com.dsrj.common.model.SysLog;
import com.dsrj.system.annotation.Log;
import com.dsrj.system.controller.BaseController;
import com.dsrj.util.Ipkit;
import com.dsrj.util.JwtUtil;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * -----------------------------------
 * Describtion:日志记录  获取注解Log  有值 则记录操作日志
 * -----------------------------------
 * Creatime:2020年01月09日
 * -----------------------------------
 * Author:mouzebin MileZL
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class LogInterceptor implements Interceptor {

    @Inject
    private SysLog sysLog;

    @Override
    public void intercept(Invocation inv) {
    	BaseController c=(BaseController)inv.getController();
        inv.invoke();
        //获取访问的方法名
        Method method = inv.getMethod();
        String methodName = method.getName();
        Log perms = method.getAnnotation(Log.class);
        //通过token获取用户id
        int id=-1;
        id= c.getUser().getId();
       if(perms!=null && perms.value()!=""){
    	   //获取当前操作时间
           String operatingTime = LocalDateTime.now().toString();
           //将数据存储到数据库中
           saveLog(id,perms.value(),operatingTime,inv); 
       }
       

    }

    /**
     * 存储日志到数据库
     * @param id
     * @param method
     * @param operatingTime
     * @param inv
     */
    @Before(Tx.class)
    private void saveLog(int id, String methodName, String operatingTime, Invocation inv) {
        try {
            Record log = new Record().set("user_id",id).set("content",methodName).set("createdate",operatingTime).set("ip",Ipkit.getRealIp(inv.getController().getRequest()) );
            Db.save("sys_log",log);
        } catch (Exception e) {
            e.printStackTrace();
            //存储日志失败
            inv.getController().renderText("网络异常，请重试");
        }
    }

}
