package com.dsrj.system.interceptor;

import com.dsrj.system.exception.MsgException;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;
import com.jfinal.log.Log;

import io.jsonwebtoken.ExpiredJwtException;

/**
-----------------------------------
*Describtion:统一异常拦截处理
*-----------------------------------
*Creatime:2020年1月16日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class ExceInterceptor implements Interceptor {

	private static Log log=Log.getLog(ExceInterceptor.class);
	
	@Override
	public void intercept(Invocation inv) {

		try {
			inv.invoke();
		} catch (MsgException e) {
			inv.getController().renderJson(Ret.fail("msg", e.getMessage()));
			e.printStackTrace();
			log.info(e.getMessage(), e);
		}catch(ExpiredJwtException e){
			inv.getController().renderJson(Ret.fail("msg", e.getMessage()));
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(),e);
	 
			if(isAjaxRequest(inv)) {
				inv.getController().renderJson(Ret.fail("msg", "出bug了！"));
			}else {
				inv.getController().renderError(404);
			}
		}
	
	}
	
	public boolean isAjaxRequest(Invocation inv) {
		return "XMLHttpRequest".equalsIgnoreCase(inv.getController().getRequest().getHeader("X-Requested-With"));
	}

}
