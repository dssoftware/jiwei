package com.dsrj.system.interceptor;

import com.dsrj.system.controller.BaseController;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.Ret;

/**
-----------------------------------
*Describtion:登录拦截器 用来判断初级   ：用户登录 填写的信息是否完全及格式 是否正确
*-----------------------------------
*Creatime:2020年1月8日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class LoginInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		//获取Token信息 存入全局、解密得到用户信息存入全局、通过用户信息获取用户权限存入全局
		BaseController c=(BaseController)inv.getController();
		if(c.isAjax()) {
			inv.invoke();
			return ;
		}
		String username=c.get("username");
		String password=c.get("password");
		if(username==null || username =="" ){
			c.renderJson(Ret.fail("msg", "用户名不能为空！"));
			return ;
		}
		if(password==null || password =="" ){
			c.renderJson(Ret.fail("msg", "密码不能为空！"));
			return ;
		}
		inv.invoke();
	}

}
