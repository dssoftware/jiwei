package com.dsrj.system.login;

import java.util.Map;

import com.dsrj.common.model.SysUser;
import com.dsrj.util.JwtUtil;
import com.jfinal.aop.Inject;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.DaoTemplate;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

/**
-----------------------------------
*Describtion:登录管理
*-----------------------------------
*Creatime:2020年1月16日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class LoginService {

	@Inject
	private SysUser dao;
	/**
	 * 用户登录
	 * @param username
	 * @param pwd
	 * @param ip
	 * @return
	 */
	public Ret login(String username, String pwd, String ip) {
		System.out.println(username);
		Kv vals=Kv.by("username",username);
		Record loginUser=Db.findFirst("select * from sys_user where username=?",username);
//		SysUser loginUser=r.C
		if(loginUser==null) {
			new Ret();
			return Ret.fail("msg", "用户不存在!");
		}
		String dbPwd=loginUser.get("password").toString();
		String loginPwdMd5=pwd;
 
		if(!dbPwd.equals(loginPwdMd5)) {
			new Ret();
			return new Ret().set("msg", "账号密码错误!");
		}
		loginUser.set("password", 0);
		long liveMillis=60*60*1000;
		Map<String,Object> jwt=JwtUtil.createJWTwithTime(liveMillis, loginUser.get("id").toString(), username, pwd);
		
		return Ret.ok("token",jwt.get("token")).set("deadline",jwt.get("deadline"));
	}

}
