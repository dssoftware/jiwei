package com.dsrj.system.login;

import com.dsrj.system.controller.BaseController;
import com.dsrj.util.Ipkit;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;

/**
-----------------------------------
*Describtion:用户登录
*-----------------------------------
*Creatime:2020年1月15日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/
public class LoginController extends BaseController {

	@Inject
	private LoginService loginService;
	
	/**
	 * 用户登录
	 */
	public void index(){
		
		if(isLogin()) {
			renderJson(Ret.ok());
		}
		
		String username=getPara("username");
		String pwd=getPara("password");
		String ip=Ipkit.getRealIp(getRequest());
		Ret ret=loginService.login(username,pwd,ip);
		renderJson(ret);
	}
}
