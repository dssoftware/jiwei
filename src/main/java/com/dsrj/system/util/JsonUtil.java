package com.dsrj.system.util;

import com.alibaba.fastjson.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.lang.reflect.Field;

/**
 * @Author 邢方辉
 * @Date 2020/1/10 8:48
 * @Version 1.0
 * @Content 创建一个java文本
 */
public class JsonUtil implements Job {
    //将json对象转换为实体类
    public static Object JsonToObject(JSONObject json,Object object) throws IllegalAccessException, InstantiationException {
        Class cla=object.getClass();
        Field[] fields=cla.getDeclaredFields();
            Object objRow=cla.newInstance();
            for (Field field : fields) {
                String name=field.getName();
                field.setAccessible(true);
                field.set(objRow, json.get(name));
            }
        return objRow;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("11111");
    }

    //将实体类对象转换成json对象

    //将json对象字符串转换为json
}
