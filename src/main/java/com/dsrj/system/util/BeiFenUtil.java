package com.dsrj.system.util;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author 邢方辉
 * @Date 2020/1/10 9:40
 * @Version 1.0
 * @Content 处理数据库备份与还原任务
 */
public class BeiFenUtil implements Job {
    public static Boolean BeiFen(){
        LocalDateTime date = LocalDateTime.now();
        File file=new File("");
        String[] arr=file.getAbsolutePath().split("\\\\");
        String filepath="";
        for(String s:arr){
            filepath=filepath+s+"\\";
        }
        String user = "root"; // 数据库帐号
        String password = "root"; // 登陆密码
        String database = "xing"; // 需要备份的数据库名
/*        String filepath = path+"数据库备份.sql"; // 备份的路径地址*/
        String stmt1 = "mysqldump " + database + " -u " + user + " -p"
                + password + " --result-file=" + filepath+"sql\\"+"数据库备份_"+date.getMonth()+date.getDayOfMonth()+date.getHour()+".sql";
        try {
            Runtime.getRuntime().exec(stmt1);
            return true;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    public static Boolean Recover(String path){
        String filepath = path+"数据库备份.sql"; // 备份的路径地址
        //新建数据库test
        String stmt1 = "mysqladmin -u root -proot create test";
        String stmt2 = "mysql -u root -proot test < " + filepath;
        String[] cmd = { "cmd", "/c", stmt2 };

        try {
            Runtime.getRuntime().exec(stmt1);
            Runtime.getRuntime().exec(cmd);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        BeiFen();
        System.out.println("2222");
    }
}
