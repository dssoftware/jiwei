package com.dsrj.system.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
-----------------------------------
*Describtion:全局拦截器  可以修改request 对URL地址做一个编码的事情、过滤掉没用的参数、安全校验
*-----------------------------------
*Creatime:2020年1月8日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*
*在进入servlet之前的拦截
*用户授权的Filter：Filter负责检查用户请求，根据请求过滤用户非法请求。
*日志Filter：详细记录某些特殊的用户请求。
*负责解码的Filter:包括对非标准编码的请求解码。
*能改变XML内容的XSLT Filter等。
*Filter可以负责拦截多个请求或响应；一个请求或响应也可以被多个Filter拦截。
*-----------------------------------
**/
public class GlobolFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest)request;
		
		String path=req.getServletPath();
		
		if(path.endsWith(".html")) {
			request.getRequestDispatcher("/sys/error/404.html").forward(request, response);
			return;
		} 
		chain.doFilter(request, response);

	}

}
