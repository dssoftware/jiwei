package com.dsrj.system.filter;

import com.dsrj.common.entity.ConstCode;
import com.dsrj.common.entity.RD;
import com.dsrj.system.util.BeiFenUtil;
import com.dsrj.util.QuartzManager;

import javax.servlet.*;
import javax.servlet.Filter;
import java.io.IOException;

/**
 * @Author 邢方辉
 * @Date 2020/1/11 13:24
 * @Version 1.0
 * @Content 用于开启数据库备份线程启动
 */
public class MenuFilter implements Filter {
    static {
       /* QuartzManager.addJob("fenyetime", BeiFenUtil.class,"1-7 4/1 0-23 1-31 * ? ");
        System.out.println("开启Fne");
        QuartzManager.startJobs();*/
    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        QuartzManager.shutdownJobs();
    }
}
