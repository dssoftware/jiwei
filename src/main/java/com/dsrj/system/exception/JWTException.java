package com.dsrj.system.exception;
/**
-----------------------------------
*Describtion:重写jwt的错误机制
*-----------------------------------
*Creatime:2019年3月7日
*-----------------------------------
*Author:Mile Chan
*-----------------------------------
*Comment:
*-----------------------------------
**/

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;

public class JWTException extends ExpiredJwtException{

	private static final long serialVersionUID = 1L;
	

	public JWTException(Header header, Claims claims, String message) {
		super(header, claims, message);
		// TODO Auto-generated constructor stub
	}
	

	
}
