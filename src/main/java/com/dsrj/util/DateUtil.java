package com.dsrj.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * -----------------------------------
 * Describtion:时间工具类
 * -----------------------------------
 * Creatime:2019年3月15日
 * -----------------------------------
 * Author:Mile Chan
 * -----------------------------------
 * Comment:
 * -----------------------------------
 **/
public class DateUtil {

    /**
     * 获取当前时间 并格式化为 2019-03-16 12:42:26形式
     *
     * @return datetime
     */
    public static String getNowDatetoString() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /**
     * 获取当前时间 并格式化为 2019-03-16形式
     *
     * @return date
     */
    public static String getNowDatetoString2() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 将默认的Date形式 转为 2019-03-16 12:42:26形式
     *
     * @param date 时间对象
     * @return datetime
     */
    public static String parseDatetoString(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }


    /**
     * 字符串转时间
     *
     * @param date   时间对象
     * @param format 时间格式
     * @return date
     * @throws ParseException 解析异常
     */
    public static Date prase(String date, String format) throws ParseException {
        return new SimpleDateFormat(format).parse(date);
    }

    /**
     * 时间转字符串
     *
     * @param date   时间对象
     * @param format 时间格式
     * @return date
     */
    public static String format(Date date, String format)  {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 将时间戳转换为时间
     */
    public static String stampToDate(String time) {
        Long timeLong = Long.parseLong(time);
        //要转换的时间格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        try {
            date = sdf.parse(sdf.format(timeLong));
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据当前日期获得所在月的日期区间
     */
    public static List<String> getTimeMonth() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate today = LocalDate.now();
        int days = today.lengthOfMonth();
        List<String> month = new ArrayList<>();
        for (int i = 1; i <= days; i++) {
            month.add(today.withDayOfMonth(i).format(fmt));
        }
        return month;
    }

    /**
     * 根据当前日期获得所在周的日期区间
     */
    public static List<String> getTimeWeek() {
        LocalDate today = LocalDate.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        ArrayList<String> week = new ArrayList<>();
        week.add(today.with(DayOfWeek.MONDAY).format(fmt));
        week.add(today.with(DayOfWeek.TUESDAY).format(fmt));
        week.add(today.with(DayOfWeek.WEDNESDAY).format(fmt));
        week.add(today.with(DayOfWeek.THURSDAY).format(fmt));
        week.add(today.with(DayOfWeek.FRIDAY).format(fmt));
        return week;
    }


    public static String getYMDHMSW() {
        Calendar calendar = Calendar.getInstance();
        String year = String.valueOf(calendar.get(Calendar.YEAR)).substring(2, 4);
        String month = "0" + (calendar.get(Calendar.MONTH) + 1);
        int d = calendar.get(Calendar.DATE);
        String day = d >= 10 ? String.valueOf(d) : "0" + d;
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        String hours = h >= 10 ? String.valueOf(h) : "0" + h;
        int m = calendar.get(Calendar.MINUTE);
        String min = m >= 10 ? String.valueOf(m) : "0" + m;
        int s = calendar.get(Calendar.SECOND);
        String sec = s >= 10 ? String.valueOf(s) : "0" + s;
        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1 < 0 ? 0 : calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (week == 0) week = 7;
        return year  + month + day + hours + min + sec+ "0" + week;
    }
    
    /**
     * 获取时分秒 并转化为16进制
     * @return
     */
    public static String getYMDHMSWToHex() {
        Calendar calendar = Calendar.getInstance();
        String yearo = String.valueOf(calendar.get(Calendar.YEAR)).substring(2, 4);
        int yearn=Integer.parseInt(yearo);
        String year=yearn>15? Integer.toHexString(yearn):"0" +Integer.toHexString(yearn) ;
        
        int mn=(calendar.get(Calendar.MONTH) + 1);
        String month =mn>15?Integer.toHexString(mn):"0" +Integer.toHexString(mn) ;
        
        int d = calendar.get(Calendar.DATE);
        String day = d > 15 ? Integer.toHexString(d) : "0" + Integer.toHexString(d);
        
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        System.out.println("week"+h);
        String hours = h > 15 ? Integer.toHexString(h) : "0" + Integer.toHexString(h);
        
        int m = calendar.get(Calendar.MINUTE);
        String min = m > 15 ? Integer.toHexString(m) : "0" + Integer.toHexString(m);
        
        int s = calendar.get(Calendar.SECOND);
        String sec = s > 15 ? Integer.toHexString(s) : "0" +  Integer.toHexString(s);
        
        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1 < 0 ? 0 : calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (week == 0) week = 7;
        return year  + month + day + hours + min + sec+ "0" + week;
    }

    public static List<String> findDates(Date dStart, Date dEnd) {
        List<String> lDate = new ArrayList<>();
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        lDate.add(sd.format(dStart));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dStart);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime()))
        {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(sd.format(calBegin.getTime()));
        }
        return lDate;
    }


    public static void main(String[] args) {
//        try {
//            System.out.println(format(prase("2019-04-29 16:25:17","yyyy-mm-dd HH:mm:ss"),"yyyy-MM-dd"));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    	System.out.println(getYMDHMSWToHex());
    }
}
