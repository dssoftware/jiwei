package com.dsrj.util;

import java.util.List;
import java.util.Map;

import com.dsrj.system.service.JobManagerService;
import com.dsrj.system.util.BeiFenUtil;
import com.jfinal.plugin.activerecord.Record;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * ----------------------------------- 
 * Describtion:定时器工具类
 * ----------------------------------- 
 * Creatime:2018年8月2日
 * -----------------------------------
 *  Author:Mile Chan
 * ----------------------------------- 
 * Comment:
 * -----------------------------------
 **/

public class QuartzManager {
    private static SchedulerFactory gSchedulerFactory = new StdSchedulerFactory(); // 创建一个SchedulerFactory工厂实例
    private static String JOB_GROUP_NAME = "FH_JOBGROUP_NAME"; // 任务组
    private static String TRIGGER_GROUP_NAME = "FH_TRIGGERGROUP_NAME"; // 触发器组
    private static Scheduler scheduler = null;
    static{
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
    //初始化定时任务
    public void initJob(List<Record> jobs) {
        for (Record job : jobs) {
            JobManagerService.pan_duan(job);
        }
        this.startJobs();

    }

    /**
     * 添加一个定时任务，使用默认的任务组名，触发器名，触发器组名
     *
     * @param jobName
     *            任务名
     * @param cls
     *            任务
     * @param time
     *            时间设置，参考quartz说明文档
     */
    public static void addJob(String jobName, Class<? extends Job> cls,
                              String time) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler(); // 通过SchedulerFactory构建Scheduler对象
            JobDetail jobDetail = newJob(cls)
                    .withIdentity(jobName, JOB_GROUP_NAME).build(); // 用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
            CronTrigger trigger = (CronTrigger) newTrigger() // 创建一个新的TriggerBuilder来规范一个触发器
                    .withIdentity(jobName, TRIGGER_GROUP_NAME) // 给触发器起一个名字和组名
                    .withSchedule(cronSchedule(time))
                    .build();
            scheduler.scheduleJob(jobDetail, trigger);
            if (!scheduler.isShutdown()) {
                System.out.println("4");
                scheduler.start(); // 启动
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println("6");
    }


    /**
     * 添加一个定时任务，使用默认的任务组名，触发器名，触发器组名 （带参数）
     *
     * @param jobName
     *            任务名
     * @param cls
     *            任务
     * @param time
     *            时间设置，参考quartz说明文档
     */
    public static void addJob(String jobName, Class<? extends Job> cls,
                              String time, Map<String, Object> parameter) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler(); // 通过SchedulerFactory构建Scheduler对象
            JobDetail jobDetail = newJob(cls)
                    .withIdentity(jobName, JOB_GROUP_NAME).build(); // 用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
            jobDetail.getJobDataMap().put("parameterList", parameter); // 传参数
            CronTrigger trigger = (CronTrigger) newTrigger() // 创建一个新的TriggerBuilder来规范一个触发器
                    .withIdentity(jobName, TRIGGER_GROUP_NAME) // 给触发器起一个名字和组名
                    .withSchedule(cronSchedule(time))
                    .build();
            scheduler.scheduleJob(jobDetail, trigger);
            if (!scheduler.isShutdown()) {
                scheduler.start(); // 启动
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 添加一个定时任务
     *
     * @param jobName
     *            任务名
     * @param jobGroupName
     *            任务组名
     * @param triggerName
     *            触发器名
     * @param triggerGroupName
     *            触发器组名
     * @param jobClass
     *            任务
     * @param time
     *            时间设置，参考quartz说明文档
     */
    public static void addJob(String jobName, String jobGroupName,
                              String triggerName, String triggerGroupName,
                              Class<? extends Job> jobClass,
                              String time) {
        System.out.println("3");
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            JobDetail jobDetail = newJob(jobClass)
                    .withIdentity(jobName, jobGroupName).build();// 任务名，任务组，任务执行类
            CronTrigger trigger = (CronTrigger) // 触发器
                    newTrigger()
                    .withIdentity(triggerName, triggerGroupName)
                    .withSchedule(cronSchedule(time))
                    .build();
            scheduler.scheduleJob(jobDetail, trigger);
            if (!scheduler.isShutdown()) {
                scheduler.start(); // 启动
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 添加一个定时任务 （带参数）
     *
     * @param jobName
     *            任务名
     * @param jobGroupName
     *            任务组名
     * @param triggerName
     *            触发器名
     * @param triggerGroupName
     *            触发器组名
     * @param jobClass
     *            任务
     * @param time
     *            时间设置，参考quartz说明文档
     */
    public static void addJob(String jobName, String jobGroupName,
                              String triggerName, String triggerGroupName,
                              Class<? extends Job> jobClass,
                              String time, Map<String, Object> parameter) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            JobDetail jobDetail = newJob(jobClass)
                    .withIdentity(jobName, jobGroupName).build();// 任务名，任务组，任务执行类
            jobDetail.getJobDataMap().put("parameterList", parameter); // 传参数
            CronTrigger trigger = (CronTrigger) // 触发器
                    newTrigger()
                    .withIdentity(triggerName, triggerGroupName)
                    .withSchedule(cronSchedule(time))
                    .build();
            scheduler.scheduleJob(jobDetail, trigger);
            if (!scheduler.isShutdown()) {
                scheduler.start(); // 启动
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 修改一个任务的触发时间(使用默认的任务组名，触发器名，触发器组名)
     *
     * @param jobName
     *            任务名
     * @param time
     *            新的时间设置
     */
    public static void modifyJobTime(String jobName, String time) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler(); // 通过SchedulerFactory构建Scheduler对象
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName,
                    TRIGGER_GROUP_NAME); // 通过触发器名和组名获取TriggerKey
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey); // 通过TriggerKey获取CronTrigger
            if (trigger == null) {
                return;
            }
            String oldTime = trigger.getCronExpression();
            if (!oldTime.equalsIgnoreCase(time)) {
                JobKey jobKey = JobKey.jobKey(jobName, JOB_GROUP_NAME); // 通过任务名和组名获取JobKey
                JobDetail jobDetail = scheduler.getJobDetail(jobKey);
                Class<? extends Job> objJobClass = jobDetail.getJobClass();
                removeJob(jobName);
                addJob(jobName, objJobClass, time);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 修改一个任务的触发时间
     *
     * @param triggerName
     *            任务名称
     * @param triggerGroupName
     *            传过来的任务名称
     * @param time
     *            更新后的时间规则
     */
    public static void modifyJobTime(String triggerName,
                                     String triggerGroupName, String time) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler(); // 通过SchedulerFactory构建Scheduler对象
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName,
                    triggerGroupName); // 通过触发器名和组名获取TriggerKey
            CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey); // 通过TriggerKey获取CronTrigger
            if (trigger == null)
                return;
            CronScheduleBuilder scheduleBuilder = cronSchedule(trigger.getCronExpression());
            String oldTime = trigger.getCronExpression();
            if (!oldTime.equalsIgnoreCase(time)) {
                trigger = (CronTrigger) trigger.getTriggerBuilder() // 重新构建trigger
                        .withIdentity(triggerKey)
                        .withSchedule(scheduleBuilder)
                        .withSchedule(cronSchedule(time))
                        .build();
                scheduler.rescheduleJob(triggerKey, trigger); // 按新的trigger重新设置job执行
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 移除一个任务(使用默认的任务组名，触发器名，触发器组名)
     *
     * @param jobName
     *            任务名称
     */
    public static void removeJob(String jobName) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName,
                    TRIGGER_GROUP_NAME); // 通过触发器名和组名获取TriggerKey
            JobKey jobKey = JobKey.jobKey(jobName, JOB_GROUP_NAME); // 通过任务名和组名获取JobKey
            scheduler.pauseTrigger(triggerKey); // 停止触发器
            scheduler.unscheduleJob(triggerKey);// 移除触发器
            scheduler.deleteJob(jobKey); // 删除任务
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * 移除一个任务
     *
     * @param jobName
     *            任务名
     * @param jobGroupName
     *            任务组名
     * @param triggerName
     *            触发器名
     * @param triggerGroupName
     *            触发器组名
     */
    public static void removeJob(String jobName, String jobGroupName,
                                 String triggerName, String triggerGroupName) {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName,
                    triggerGroupName); // 通过触发器名和组名获取TriggerKey
            JobKey jobKey = JobKey.jobKey(jobName, jobGroupName); // 通过任务名和组名获取JobKey
            scheduler.pauseTrigger(triggerKey); // 停止触发器
            scheduler.unscheduleJob(triggerKey);// 移除触发器
            scheduler.deleteJob(jobKey); // 删除任务
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 启动所有定时任务
     */
    public static void startJobs() {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            scheduler.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭所有定时任务
     */
    public static void shutdownJobs() {
        try {
            //Scheduler sched = gSchedulerFactory.getScheduler();
            if (!scheduler.isShutdown()) {
                scheduler.shutdown();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

